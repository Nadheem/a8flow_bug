BASE_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
IMAGE_NAME=asia.gcr.io/a8platform/a8flow-frontend

if [ -z $1 ]; then
    IMAGE_TAG=`docker images | grep a8flow-frontend | head -1 | awk '{print $2}'`
else
    IMAGE_TAG=$1
fi

if [ -z $IMAGE_TAG ]; then
    IMAGE_TAG="1.0"
fi

echo "Building $IMAGE_NAME:$IMAGE_TAG"
npm run build && docker build -t $IMAGE_NAME:$IMAGE_TAG $BASE_DIR && docker push $IMAGE_NAME:$IMAGE_TAG
