FROM nginx:1.15.5-alpine
LABEL maintainer="Jagadish G <jagadish@autonom8.com>"

# Install system dependencies
RUN set -xe \
    && apk add --no-cache bash

# Copy build directory
RUN rm -rf /usr/share/nginx/html/*
COPY build/ /usr/share/nginx/html/

# Set working directory
WORKDIR /usr/share/nginx/html/

# Copy server configuration
COPY docker-build/nginx.conf /etc/nginx/nginx.conf
COPY docker-build/nginx.vh.default.conf /etc/nginx/conf.d/default.conf

# Copy start.sh file
COPY docker-build/start.sh /root/

# Expose port
EXPOSE 80

# Update config file & Run application
ENTRYPOINT ["/bin/bash", "/root/start.sh"]