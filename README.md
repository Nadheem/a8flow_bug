# A8flow-Web
##### Project Description here :)

### Project Core Structure
```
|src/
├── assets //Use this to store all static resources
├── components //contain all app releated components
│   ├── auth
│   ├── home
│   └── index.js
├── constants //use this folder to store immutable infos
│   ├── api.js 
│   ├── config.js
│   └── index.js
├── helpers // use this to create helpers 
│   ├── commonFun // contains all shared functional logics
│   │   ├── errorHandler
│   │   │   ├── errorBoundary.js
│   │   │   └── errorBoundary.module.css
│   │   ├── index.js
│   │   └── privateRoute.js
│   ├── commonUi // contians all shared ui components
│   │   ├── index.js
│   │   └── persistLoader.js
│   ├── form // contains all resuable form components
│   │   ├── aceReduxFormWraper.js
│   │   ├── antRFConnector.js
│   │   └── caseCaderWrapper.js
│   └── index.js
├── stateManager // this folder is heart of redux (State Management)
│   ├── actions // contains all actions seperated by functionality.
│   │   ├── auth.action.js
│   │   └── index.js
│   ├── reducers // contains all reducers seperated by functionality 
│   │   ├── auth.reducer.js
│   │   └── index.js
│   ├── types // contains all types seperated by functionality
│   │   ├── auth.type.js
│   │   └── index.js
│   └── index.js 
```

## Primary Libraries  :
 - redux (State management)
 - redux-form (Form management)
 - redux-persist (Handling Storage)
 - react-router-dom (Routing Management)
 - redux-thunk (async logic that interacts with the store)
 - axios (For web request handling)

## Available Scripts
In the project directory, you can run:
### `npm install`
### `npm start`
### `npm build`
