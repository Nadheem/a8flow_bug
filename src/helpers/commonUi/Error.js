import React from 'react';
import Style from "./Error.module.css";
const Error = () => (
    <div className={`${Style.eWrapper}`}><span> Errors Found! For More Info See the console...</span></div>
);


export default Error;