import React, { Component } from 'react';
import {connect} from "react-redux";
import { Link } from "react-router-dom";
import { Tooltip } from 'antd';
import {rootSelectedMenu} from "../../../stateManager/actions";

class LeftNav extends Component {
    state = {}
    handleMenu = (selectedMenu) => {
        this.props.rootSelectedMenu(selectedMenu);
    }
    render() {
        return (
            <div className="dash-nav-main">
                <nav className="dash-nav">
                    <ul>
                        <li className={this.props.ui.activeRootNav === "dashboard" ? "active" : ""}>
                            <Tooltip placement="right" title="Dashboard">
                                <Link to="/dashboard" onClick={this.handleMenu.bind(this, "dashboard")} value="sathish">
                                    <i className="icon-dashboard"></i>
                                </Link>
                            </Tooltip>
                            {/* <a href="dashboard.html" data-toggle="tooltip" data-placement="right" title="Dashboard"><i className="icon-dashboard"></i></a> */}
                        </li>
                        <li className={this.props.ui.activeRootNav === "task" ? "active" : ""}>
                            <Tooltip placement="right" title="My Task">
                                <Link to="/task" onClick={this.handleMenu.bind(this, "task")}>
                                    <i className="icon-todo"></i>
                                </Link>
                            </Tooltip>
                        </li>
                        <li className={this.props.ui.activeRootNav === "settings" ? "active" : ""}>
                            <Tooltip placement="right" title="Settings">
                                <Link to="/settings" onClick={this.handleMenu.bind(this, "settings")}>
                                    <i className="icon-settings"></i>
                                </Link>
                            </Tooltip>
                        </li>
                    </ul>
                </nav>
                <div className="user-info">
					<div className="user-info-in">
						<img src="assets/images/temp/user.png" alt="User Name"/>
						<span>Admin</span>
					</div>
				</div>
            </div>
        );
    }
}


const mapStateToProps = (state, props) => {
    return {
        state: state.user,
        ui: state.ui
    }
}

export default connect(mapStateToProps, { rootSelectedMenu })(LeftNav);
