/**
|--------------------------------------------------
| This file is a preloader for redux-persist(local storage)
|--------------------------------------------------
*/
import React from 'react';

export default (props)=>(
    <div>
        <h3>Loading...</h3>
    </div>
);