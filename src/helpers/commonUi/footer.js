import React from 'react';
import footerImage from "../../assets/images/Autonom8.png";
const Footer = () => (
    <footer id="colophon" className="site-footer" role="contentinfo">
        <div className="container-fluid">
            <div className="row">
                <div className="col-xs-12 col-sm-6">
                    <p>Powered by <img src={footerImage} alt="Autonom8" /></p>
                </div>
            <div className="col-xs-12 col-sm-6">
                    <ul className="list-inline">
                        <li><a href="/task/">Terms of Use</a></li>
                        <li><a href="/task/">Privacy Policy</a></li>
                    </ul>
                </div>
        </div>
    </div>
</footer>
);

export default Footer;