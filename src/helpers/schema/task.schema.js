
export const _schemaGetTask = {
    "$id": "http://example.com/schemas/schema.json",
    "type": "object",
    "properties": {
        "taskName": {
            "type": "string"
        },
        "user": {
            "type": "string"
        },
        "authToken": {
            "type": "string"
        },
        "filter": {
            "type": "object",
        },
        "resultPointer": {
            "type": "number"
        },
        "offset": {
            "type": "number",
        },
        "type": {
            "type": "string"
        }
    },
    "required": [
        "taskName", "user", "authToken", "filter", "resultPointer", "offset", "type"
    ],
    "additionalProperties": false,
    "errorMessage": {
        "properties": {
            "taskName": "taskName param type is invalid & required STRING type @getTask()",
            "user": "user param type is invalid & required STRING type @getTask()",
            "authToken": "authToken param type is invalid & required STRING type @getTask()",
            "filter": "filter param type is invalid & required Object type @getTask()",
            "resultPointer": "resultPointer param type is invalid & required NUMBER type @getTask()",
            "offset": "offset param type is invalid & required NUMBER type @getTask()",
            "type": "type param type is invalid & required STRING type @getTask()"
        },
        "additionalProperties": "Hi I am getTask() fun please provide what i need.. do not provide extra params",
        //for single required props
        //"required" : 'schema should have title props'

        "required": {
            "taskName": "taskName param is missing @getTask()",
            "user": "user param is missing @getTask()",
            "authToken": "authToken param is missing @getTask()",
            "filter": "filter param is missing @getTask()",
            "resultPointer": "resultPointer param is missing @getTask()",
            "offset": "taskoffsetName param is missing @getTask()",
            "type": "type param is missing @getTask()"
        }
    }
};