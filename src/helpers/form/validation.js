import Validate from "validate.js";

const required = value => Validate.isEmpty(value) ? 'Required!' : undefined;

export default {
    required
}