/**
 * Wrapper for Antdesign component with redux
 * created by a8
 */

import React from 'react';
import { Form } from "antd";
import { Input, Select, Cascader, Radio, Upload, Switch } from "antd"

export const RenderField = Component => ({ input, meta, children = null, formItemStyle = {}, formItemLayout = {}, hasFeedback, label, ...rest }) => {
  const hasError = meta.touched && meta.invalid;
  return (
    <Form.Item
      {...formItemLayout}
      label={label ? label : null}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error}
      style={{ ...formItemStyle }}
    >
      <Component {...input}  {...rest} children={children}
        defaultChecked={input.value}
      />
    </Form.Item>
  );
};

export const CascaderHelper = (props) => {
  const { input, cascaderProps, formItemStyle, meta, label = null, hasFeedback } = props;
  const hasError = meta.dirty && meta.invalid;
  return (
    <Form.Item
      label={label ? label : null}
      validateStatus={hasError ? "error" : "success"}
      hasFeedback={hasFeedback && hasError}
      help={hasError && meta.error}
      style={{ ...formItemStyle }}
    >
      <Cascader
        value={input.value ? input.value : undefined}
        onChange={input.onChange}
        {...cascaderProps}
      />
    </Form.Item>
  );
}


//connect antd element with redux-form
export const TextBox = RenderField(Input);

//conenct antd element with redux-form
export const TextAreaHelper = RenderField(Input.TextArea);

//select component
export const SelectHelper = RenderField(Select);

//Radio.Group Wrapper 
export const RadioWrapper = RenderField(Radio.Group);

//Upload wrapper with redux-form
export const UploadHelper = RenderField(Upload);

//Switch wrapper
export const SwitchWrapper = RenderField(Switch);