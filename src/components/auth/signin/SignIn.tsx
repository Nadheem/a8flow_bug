import * as React  from "react";
import { Form, Button } from "antd";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { TextBox, Validate } from "../../../helpers";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
import { connect } from "react-redux";
import { signin } from "../../../stateManager/actions";
// import Style from "./SignIn.module.css";
import logo from "../../../assets/images/logo.svg";
import loginImg from "../../../assets/images/login-image.jpg";
import { SubmissionError } from "redux-form";
import {AppState} from "../../../stateManager/reducers";

type SigninProps = {
  signin: Function;
} & InjectedFormProps &
  RouteComponentProps;

type SignInState = {
  btnLoading: boolean;
  result: any
};

class Signin extends React.Component<SigninProps, SignInState> {
  iAmAlive: Boolean;
  constructor(props: any) {
    super(props);
    this.state = {
      result: null,
      btnLoading: false
    };
    this.iAmAlive = false;
  }
  async componentDidMount() {
    console.log("From SignIn");
    this.iAmAlive = true;
  }

  handleForm = async (values: any) => {
    try {
      this.setState({ btnLoading: true });
      await this.props.signin(values);
      this.props.history.push("/task");
    } catch (error) {
      throw new SubmissionError({ _error: error.response.data.message });
    } finally {
      if (this.iAmAlive) {
        this.setState({ btnLoading: false });
      }
    }
  };

  componentWillUnmount() {
    this.iAmAlive = false;
  }

  render() {
    const { handleSubmit, error } = this.props;
    return (
      <div className="page-login">
        <div className="container-fluid">
          <div className="flex-main">
            <div className="col-xs-12 col-sm-6 flex-item site-login-image-wrap">
              <div
                className="site-login-image"
                style={{ backgroundImage: `url(${loginImg})` }}
              >
                <div className="site-login-image-main">
                  <div className="site-login-image-in">
                    <h1>
                      Get things <br />
                      done.
                    </h1>
                    <p>Process automation for Enterprises</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-sm-6 flex-item">
              <div className="site-login">
                <div className="site-login-in">
                  <img src={logo} alt="ESAF" />
                  <h2>Login</h2>
                  <Form
                    layout={"vertical"}
                    className="login-form"
                    onSubmit={handleSubmit(this.handleForm)}
                  >
                    <Field
                      label={"User Name"}
                      name="username"
                      component={TextBox}
                      placeholder="Enter Email"
                      type="text"
                      hasFeedback
                      className="form-control-coustom"
                      validate={[Validate.required]}
                    />

                    <Field
                      label={"Password"}
                      name="password"
                      component={TextBox}
                      placeholder="Enter Password"
                      type="password"
                      hasFeedback
                      className="form-control-coustom"
                      validate={[Validate.required]}
                    />

                    <Button
                      type="primary"
                      htmlType="submit"
                      className="button button-primary"
                      style={{ marginTop: "9px" }}
                      loading={this.state.btnLoading}
                    >
                      LOG IN
                    </Button>
                    <br />
                    <br />
                    {error && (
                      <strong style={{ color: "#CB1E1A" }}>{error}</strong>
                    )}
                  </Form>
                  <p>
                    <Link to="/forgotpassword">Forgot Password ?</Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    state: state.user
  };
};

export default reduxForm({ form: "signin" })(
  withRouter(
    connect(
      mapStateToProps,
      { signin }
    )(Signin)
  )
);
