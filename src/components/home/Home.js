import React, { Component } from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";
import {
  getAllTaskCounts,
  handleTaskFromVisibility
} from "../../stateManager/actions";
import { MyTask, GroupTask, UnClaimedTask, TaskNav } from "../task";
import MainHeader from "../common/MainHeader";
import { TaskDrawers } from "../task/sharedTaskComponents";

class Home extends Component {
  state = {
    selectedMenu: "mytask"
  };

  componentDidMount() {
    //get initial tasks counts using below action
    this.props.getAllTaskCounts({
      authToken: `Bearer ${this.props.user.info.authToken}`
    });

    this.props.handleTaskFromVisibility(false);
  }

  render() {
    return (
      <MainHeader>
        <TaskNav />
        <Route
          exact
          path={["/", "/task/", `/task/myTask`]}
          component={MyTask}
        />
        <Route path={"/task/groupTasks/"} component={GroupTask} />
        <Route path={"/task/unclaimedTasks"} component={UnClaimedTask} />

        {
          // drawer wrapper
        }
        {/* <div className="card-item-details-wrap"> */}
        {/* <TaskDrawer /> */}
        {/* </div> */}

        {
          // end drawer wrapper
        }
        <TaskDrawers />
      </MainHeader>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    user: state.user,
    ui: state.ui,
    task: state.task
  };
};

export default connect(
  mapStateToProps,
  {
    getAllTaskCounts,
    handleTaskFromVisibility
  }
)(Home);
