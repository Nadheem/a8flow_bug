import React, { Component } from 'react';
import { TaskNav, TaskUtility, TaskFilter } from "../task";

class Header extends Component {
    state = {}
    render() {
        return (
            <div className="page-head">
                <TaskNav />
                <TaskUtility />
                <TaskFilter />
            </div>
        );
    }
}

export default Header;