import React from 'react';
import { connect } from "react-redux";
import className from "classnames";
import logo from "../../assets/images/logo.svg";
import { Link } from "react-router-dom";
import { rootSelectedTab } from "../../stateManager/actions";

export interface ISettingsNavProps {
    
}
 
export interface ISettingsNavState {
    
}
 
class SettingsNav extends React.Component<any, ISettingsNavState> {
    constructor(props: ISettingsNavProps) {
        super(props);
        this.state = {  };
    }

    handleTab = (key:any) => {
        this.props.rootSelectedTab(key);
    }

    render() { 
        return (
            <div className="page-head">
                <div className="page-head-main">
                    <img src={logo} alt="ESAF" className="site-logo" />
                    <div className="container-fluid">
                        <nav className="tab-list">
                            <ul className={true ? "dfdfds" : "dfd"}>
                                <li className={className({ "active": this.props.ui.activeRootTab === "profile" })} onClick={this.handleTab.bind(this, "profile")}>
                                    <Link to={`/settings/profile`}>Profile</Link>
                                </li>
                                <li className={className({ "active": this.props.ui.activeRootTab === "changePassword" })} onClick={this.handleTab.bind(this, "changePassword")} >
                                    <Link to={`/settings/changePassword/`}>Change Password</Link>
                                </li>
                                <li className={className({ "active": this.props.ui.activeRootTab === "myGroups" })} onClick={this.handleTab.bind(this, "myGroups")}>
                                    <Link to={`/settings/myGroups`}>My Groups</Link>
                                </li>
                                <li className={className({ "active": this.props.ui.activeRootTab === "myTenants" })} onClick={this.handleTab.bind(this, "myTenants")}>
                                    <Link to={`/settings/myTenants`}>My Tenants</Link>
                                </li>
                                <li onClick={this.handleTab.bind(this, "unclaimedTasks")}>
                                    <a href="#" className="logout"><i className="icon-logout"></i>Logout</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        state: state.user,
        ui: state.ui,
        task : state.task
    }
}
export default connect(mapStateToProps, { rootSelectedTab })(SettingsNav);