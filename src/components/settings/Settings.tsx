import React from "react";
import MainHeader from "../common/MainHeader";
import { connect } from "react-redux";
import { rootSelectedMenu, rootSelectedTab } from "../../stateManager/actions";
import { signout } from "../../stateManager/actions";
import SettingsNav from "./SettingsNav";
import { Route } from "react-router-dom";
import Profile from "./profile/Profile";
import MyGroups from "./myGroups/MyGroups";
import MyTenants from "./myTenants/MyTanants";
import ChangePassword from "./changePassword/ChangePassword";

export interface ISettingsProps {}

export interface ISettingsState {
}

class Settings extends React.Component<ISettingsProps, ISettingsState> {
  state = {};
  render() {
    return (<MainHeader>
      <SettingsNav />

      <Route exact path={["/","/settings","/settings/profile"]} component={Profile} />
      <Route path="/settings/changepassword" component={ChangePassword} />
      <Route path="/settings/mygroups" component={MyGroups} />
      <Route path="/settings/mytenants" component={MyTenants} />
    </MainHeader>);
  }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.user,
    ui: state.ui,
    task: state.task
  };
};

export default connect(
  mapStateToProps,
  {
    rootSelectedMenu,
    rootSelectedTab,
    signout
  }
)(Settings);
