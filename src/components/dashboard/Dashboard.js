import React, { Component } from "react";
// import { LeftNav, Footer } from "../../helpers";
import logo from "../../assets/images/logo.svg";
import DashboardCards from "./DashboardCards";
import { Route } from "react-router-dom";
import Users from "./Users";
import Groups from "./Groups";
import Authorisations from "./Authorisations";
import System from "./System";
import Tenants from "./Tenants";
import MainHeader from "../common/MainHeader";

class Dashboard extends Component {
  state = {};
  render() {
    return (
      <MainHeader>
        <div className="page-head">
          <div className="page-head-main">
            <img src={logo} alt="ESAF" className="site-logo" />
            <div className="container-fluid">
              <h1>{"Admin Module"}</h1>
            </div>
          </div>
          <div className="page-head-top" />
          <div className="page-head-bottom" />
        </div>
        <div className="page-content-main">
          <div className="container-fluid">
            <div className="flex-row flex-row-s">
              <DashboardCards
                logoSrc="assets/images/users.svg"
                navigateTo={null /* "/dashboard/users" */}
                name="Users"
              />

              <DashboardCards
                logoSrc="assets/images/tenants.svg"
                navigateTo={null /* "/dashboard/tenants" */}
                name="Tenants"
              />

              <DashboardCards
                logoSrc="assets/images/groups.svg"
                navigateTo={null /* "/dashboard/groups" */}
                name="Groups"
              />

              <DashboardCards
                logoSrc="assets/images/authorizations.svg"
                navigateTo={null /* "/dashboard/authorizations" */}
                name="Authorizations"
              />

              <DashboardCards
                logoSrc="assets/images/system.svg"
                navigateTo={null /* "/dashboard/system" */}
                name="System"
              />

              <Route path={"/dashboard/users"} component={Users} />
              <Route path={"/dashboard/tenants"} component={Tenants} />
              <Route path={"/dashboard/groups"} component={Groups} />
              <Route
                path={"/dashboard/authorizations"}
                component={Authorisations}
              />
              <Route path={"/dashboard/system"} component={System} />
            </div>
          </div>
        </div>
      </MainHeader>
    );
  }
}

export default Dashboard;
