import * as React from "react";
import { Redirect, Link } from "react-router-dom";

export interface IDashboardCardsProps {
  navigateTo: string;
  logoSrc: string;
  name: string;
}

const DashBoardCards = (props: IDashboardCardsProps) => {
  return (
    <div className="flex-item dash-card">
      <Link to={props.navigateTo}>
        <div className="flex-inner dash-card-in">
          <img src={props.logoSrc} alt={props.name} />
          <h3>{props.name}</h3>
        </div>
      </Link>
    </div>
  );
};

export default DashBoardCards;
