import React, { Component } from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { TaskUtility } from "../sharedTaskComponents";
import moment from "moment-timezone";
import type from "../../../stateManager/types";
import Style from "./MyTask.module.css";
import { message } from "antd";
import {
  getTasks,
  resetTask,
  selectedTask,
  resetSelectedTask,
  addSelectedTaskToIframe,
  removeIframeConfig,
  addIframeUrl,
  handleIframeLoader,
  handleTaskFromVisibility
} from "../../../stateManager/actions";
import { Error } from "../../../helpers";
import { taskConstant } from "../../../constants";
// import { Axiosplus } from "../../../helpers";
class MyTask extends Component {
  state = {
    loading: true,
    hasMore: true,
    isError: false
  };
  async componentDidMount() {
    try {
      /**
       *  trigger myTask api with auth token
       **/
      this.setState({
        loading: true
      });
      // user, authToken, filter = null, offset = 10
      await this.props.getTasks({
        taskName: taskConstant.myTask,
        user: this.props.user.info.username,
        authToken: `Bearer ${this.props.user.info.authToken}`,
        filter: this.props.task.myTask.filter,
        resultPointer: taskConstant.paginationStartingPointer,
        offset: taskConstant.paginationOffsetLimit,
        type: type.GET_MY_TASKS
      });
      this.setState({
        loading: false
      });
    } catch (error) {
      this.setState({
        isError: true
      });
      console.error(error);
    } finally {
      this.setState({
        loading: false
      });
    }
  }

  onClose = () => {
    this.props.removeIframeConfig();
    this.props.resetSelectedTask();
    this.props.handleTaskFromVisibility(false);
    // if (this.props.iframe.pointer) {
    //   this.props.iframe.pointer.contentWindow.postMessage(
    //     { formAction: "reset" },
    //     Config.TargetOrigin
    //   );
    // }
  };

  // this fun handle the infiniteScroll method.
  fetchMoreTasks = () => {
    if (
      this.props.task.myTask.taskList.length >= this.props.task.myTask.count
    ) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getTasks({
      taskName: taskConstant.myTask,
      user: this.props.user.info.username,
      authToken: `Bearer ${this.props.user.info.authToken}`,
      filter: this.props.task.myTask.filter,
      resultPointer: this.props.task.myTask.taskList.length,
      offset: taskConstant.paginationOffsetLimit,
      type: type.FETCH_MORE_TASKS
    });
  };

  handleTask = async data => {
    if (data.formKey) {
      let [brand, client, processDefinition, version] = data.formKey.split(":");
      if (brand === "a8apps") {
        this.props.selectedTask({
          name: type.TASK_PROP_MYTASK,
          info: data
        });

        //get base url
        this.props.addSelectedTaskToIframe({
          name: type.TASK_PROP_MYTASK,
          info: data
        });

        //visibile the TaskForm
        this.props.handleTaskFromVisibility(true);

        //update iframe loader to true
        this.props.handleIframeLoader(true);

        //update iframeUrl based on task formkey
        let buildIframeUrl = `${
          this.props.iframe.baseUrl
        }/${client}/${processDefinition}/${version}/index.html`;
        this.props.addIframeUrl(buildIframeUrl);
        // for testing purpose uncomment below url
        // this.props.addIframeUrl("http://localhost:3001");
      } else {
        //invalid brand or formKey
        message.error("Invalid form key!");
      }
    } else {
      //invalid formKey
      message.error("Invalid form key!");
    }
  };

  componentWillUnmount() {
    this.props.resetTask(type.RESET_MY_TASKS);
  }

  render() {
    if (this.state.isError) {
      return <Error />;
    }
    if (this.state.loading) {
      return (
        <React.Fragment>
          <TaskUtility taskName={"My Tasks"} />
          <div
            className="container-fluid"
            style={{ textAlign: "center", marginTop: "20px" }}
          >
            Fetching MyTasks...
          </div>
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        {/* <div class="page-head"> */}
        <TaskUtility taskName={"My Tasks"} />
        {/* </div> */}
        <div className={`page-content-main`}>
          <div className="container-fluid">
            <div className="page-list-main">
              {this.props.task.myTask.count > 0 ? (
                <InfiniteScroll
                  dataLength={this.props.task.myTask.taskList.length}
                  next={this.fetchMoreTasks}
                  hasMore={this.state.hasMore}
                  loader={
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3 flex-item">
                      {/* <span className={Style.loading}>Loading...</span> */}
                    </div>
                  }
                  // height={"70vh"}
                  style={{ overflow: "inherit" }}
                  endMessage={
                    <p style={{ textAlign: "center", clear: "both" }}>
                      {/* <b>Yay! You have seen it all</b> */}
                    </p>
                  }
                >
                  <div className="flex-row" id="loadmore-container">
                    {this.props.task.myTask.taskList.map((data, index) => {
                      let {
                        _embedded: { variable }
                      } = data;
                      return (
                        <div
                          key={data.id}
                          className={`col-xs-12 col-sm-6 col-md-4 col-lg-3 flex-item ${
                            Style.myTask
                          }`}
                        >
                          <div
                            className="card-item card-grid-item flex-inner"
                            data-target="#card-item-details-1"
                          >
                            <label
                              className="input-check input-control"
                              htmlFor={`card-item-${index + 1}`}
                            >
                              <input
                                type="checkbox"
                                id={`card-item-${index + 1}`}
                                name=""
                              />
                              <span />
                            </label>
                            <div
                              onClick={this.handleTask.bind(this, data, index)}
                            >
                              <div className="card-grid-content">
                                <div className="card-grid-content-inner">
                                  <h3>{data.name}</h3>
                                  <ul
                                    className="list-inline card-grid-content-detail"
                                    style={{ height: "38px" }}
                                  >
                                    {variable.length > 0 ? (
                                      variable.slice(0, 2).map((v, index) => (
                                        <li key={index}>
                                          <span>{v.name}</span>
                                          {v.value}
                                        </li>
                                      ))
                                    ) : (
                                      <li />
                                    )}
                                  </ul>
                                </div>
                              </div>
                              <div className="card-grid-footer">
                                <div className="card-grid-footer-inner">
                                  <div className="card-grid-footer-content">
                                    <span className="card-grid-staus-time">
                                      {moment(data.created).fromNow()}
                                    </span>
                                    <span className="card-grid-status card-grid-status-ri">
                                      {data.processDefinitionName
                                        ? data.processDefinitionName
                                        : data.caseDefinitionName}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </InfiniteScroll>
              ) : (
                <div className="container-fluid">
                  Hi {this.props.user.info.username}. No Tasks Found For You
                </div>
              )}
            </div>
          </div>
        </div>
        {/* <TaskDrawers /> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui,
    iframe: state.iframe
  };
};

export default connect(
  mapStateToProps,
  {
    getTasks,
    resetTask,
    selectedTask,
    resetSelectedTask,
    addSelectedTaskToIframe,
    removeIframeConfig,
    addIframeUrl,
    handleIframeLoader,
    handleTaskFromVisibility
  }
)(MyTask);
