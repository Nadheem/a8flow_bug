import React, { Component } from 'react';
import { Link,Route } from "react-router-dom";
import { Tooltip, Button, Tabs } from 'antd';
import { rootSelectedMenu,rootSelectedTab } from "../../stateManager/actions";
import { connect } from "react-redux";
import { LeftNav } from "../../helpers";
import user from "../../assets/images/temp/user.png";
import logo from "../../assets/images/logo.svg";
import Style from "./Home.module.css";
import {MyTask,GroupTask,UnclaimedTask} from "../task";
import className from "classnames";
const TabPane = Tabs.TabPane;
const operations = <img src={logo} alt="ESAF" className="site-logo" />;

class Home extends Component {
    state = {
        selectedMenu: "mytask"
    }
    handleMenu = (selectedMenu) => {
        this.props.rootSelectedMenu(selectedMenu);
    }

    handleTab = (key)=>{
        console.log('from active change');
        console.log(key);
        console.log(this.props.match);
        this.props.rootSelectedTab(key);
        this.props.history.push(`${this.props.match.url}/${key}`);

    }

    render() {
        return (
            <div id="page">
                <div id="main" className="site-main" role="main">
                    <LeftNav />
                    <div className="page-wrap page-fixed-nav">
                        <div className="page-head">
                            <div className="page-head-main">
                                <div className="container-fluid">
                                    <Tabs
                                        tabBarExtraContent={operations}
                                        className="tab-list"
                                        animated={false}
                                        onChange={this.handleTab}
                                        activeKey={this.props.ui.activeRootTab}
                                        // tabBarStyle={style.tabBarStyle}
                                    >
                                        <TabPane tab={<span>My Tasks<span className={className(Style.badgeStyle,{[Style["badgeStyleActive"]] : this.props.ui.activeRootTab == "myTask" })}>15</span></span>} key="myTask">My Tasks</TabPane>
                                        <TabPane tab={<span>Group Tasks<span className={className(Style.badgeStyle,{[Style["badgeStyleActive"]] : this.props.ui.activeRootTab == "groupTasks" })}>15</span></span>} key="groupTasks">Group Tasks</TabPane>
                                        <TabPane tab={<span>Unclaimed Tasks<span className={className(Style.badgeStyle,{[Style["badgeStyleActive"]] : this.props.ui.activeRootTab == "unclaimedTasks" })}>15</span></span>} key="unclaimedTasks">Unclaimed Tasks</TabPane>
                                    </Tabs>
                                </div>
                            </div>
                            <Route path={`${this.props.match.path}/:taskType`} component={<MyTask/>} />
                        </div>
                    </div>
                </div> {/*main end here */}
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        state: state.user,
        ui: state.ui
    }
}

export default connect(mapStateToProps, { rootSelectedMenu,rootSelectedTab })(Home);

// This below object responsible for maintaing the inline style 
const inlineStyle = {
    tabBarStyle: {
        color: "#212332",
        padding: "26px 0 22px",
        display: "block",
        fontSize: "14px",
        position: "relative"
    }
}