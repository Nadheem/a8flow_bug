import React, { Component } from "react";
import { TaskUtility } from "../sharedTaskComponents";
import {
  getTasks,
  resetTask,
  selectedClaimTasks
} from "../../../stateManager/actions";
import { taskConstant } from "../../../constants";
import { connect } from "react-redux";
import type from "../../../stateManager/types";
import { Error } from "../../../helpers";
import InfiniteScroll from "react-infinite-scroll-component";
import Style from "../myTask/MyTask.module.css";
import moment from "moment-timezone";
//import { message } from "antd";

class UnClaimedTask extends Component {
  state = {
    loading: true,
    hasMore: true,
    isError: false
  };
  async componentDidMount() {
    try {
      /**
       *  trigger myTask api with auth token
       **/
      this.setState({
        loading: true
      });
      // user, authToken, filter = null, offset = 10
      await this.props.getTasks({
        taskName: taskConstant.unClaimedTasks,
        user: this.props.user.info.username,
        authToken: `Bearer ${this.props.user.info.authToken}`,
        filter: this.props.task.unClaimedTask.filter,
        resultPointer: taskConstant.paginationStartingPointer,
        offset: taskConstant.paginationOffsetLimit,
        type: type.GET_UNCLAIMED_TASKS
      });
      this.setState({
        loading: false
      });
    } catch (error) {
      this.setState({
        isError: true
      });
      console.error(error);
    } finally {
      this.setState({
        loading: false
      });
    }
  }

  componentWillUnmount() {
    this.props.resetTask(type.RESET_UNCLAIMED_TASKS);
  }

  fetchMoreTasks = () => {
    if (
      this.props.task.unClaimedTask.taskList.length >=
      this.props.task.unClaimedTask.count
    ) {
      this.setState({ hasMore: false });
      return;
    }
    this.props.getTasks({
      taskName: taskConstant.unClaimedTasks,
      user: this.props.user.info.username,
      authToken: `Bearer ${this.props.user.info.authToken}`,
      filter: this.props.task.unClaimedTask.filter,
      resultPointer: this.props.task.unClaimedTask.taskList.length,
      offset: taskConstant.paginationOffsetLimit,
      type: type.FETCH_MORE_TASKS
    });
  };

  selectTask(taskId) {
    let { selectedTasks } = this.props.task.unClaimedTask;
    if (!selectedTasks) {
      selectedTasks = [];
    }
    if (
      selectedTasks.length === 0 ||
      (selectedTasks.length > 0 && !selectedTasks.some(x => x.id === taskId))
    ) {
      selectedTasks.push({ id: taskId });
    } else {
      selectedTasks.splice(selectedTasks.indexOf({ id: taskId }), 1);
    }
    this.props.selectedClaimTasks(selectedTasks);
  }

  render() {
    if (this.state.isError) {
      return <Error />;
    }
    if (this.state.loading) {
      return (
        <React.Fragment>
          <TaskUtility taskName={"Unclaimed Tasks"} />
          <div
            className="container-fluid"
            style={{ textAlign: "center", marginTop: "20px" }}
          >
            Fetching Unclaimed Tasks...
          </div>
        </React.Fragment>
      );
    }

    let { taskList } = this.props.task.unClaimedTask;
    return (
      <React.Fragment>
        {/* <div class="page-head"> */}
        <TaskUtility taskName={"Unclaimed Tasks"} />
        {/* </div> */}
        <div className={`page-content-main`}>
          <div className="container-fluid">
            <div className="page-list-main">
              {this.props.task.unClaimedTask.count > 0 ? (
                <InfiniteScroll
                  dataLength={taskList.length}
                  next={this.fetchMoreTasks}
                  hasMore={this.state.hasMore}
                  loader={
                    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3 flex-item">
                      {/* <span className={Style.loading}>Loading...</span> */}
                    </div>
                  }
                  // height={"70vh"}
                  style={{ overflow: "inherit" }}
                  endMessage={
                    <p style={{ textAlign: "center", clear: "both" }}>
                      {/* <b>Yay! You have seen it all</b> */}
                    </p>
                  }
                >
                  <div className="flex-row" id="loadmore-container">
                    {taskList.map((data, index) => {
                      let {
                        _embedded: { variable }
                      } = data;
                      return (
                        <div
                          key={data.id}
                          className={`col-xs-12 col-sm-6 col-md-4 col-lg-3 flex-item ${
                            Style.myTask
                          }`}
                        >
                          <label
                            style={{ cursor: "pointer" }}
                            htmlFor={`card-item-${index + 1}`}
                            onChange={() => {
                              this.selectTask(data.id);
                            }}
                          >
                            <div
                              className="card-item card-grid-item flex-inner"
                              data-target="#card-item-details-1"
                            >
                              <label
                                className="input-check input-control"
                                htmlFor={`card-item-${index + 1}`}
                              >
                                <input
                                  type="checkbox"
                                  id={`card-item-${index + 1}`}
                                  name=""
                                />
                                <span />
                              </label>

                              <div
                                onClick={
                                  null /*this.handleTask.bind(this, data, index)*/
                                }
                              >
                                <div className="card-grid-content">
                                  <div className="card-grid-content-inner">
                                    <h3>{data.name}</h3>
                                    <ul
                                      className="list-inline card-grid-content-detail"
                                      style={{ height: "38px" }}
                                    >
                                      {variable.length > 0 ? (
                                        variable.slice(0, 2).map((v, index) => (
                                          <li key={index}>
                                            <span>{v.name}</span>
                                            {v.value}
                                          </li>
                                        ))
                                      ) : (
                                        <li />
                                      )}
                                    </ul>
                                  </div>
                                </div>
                                <div className="card-grid-footer">
                                  <div className="card-grid-footer-inner">
                                    <div className="card-grid-footer-content">
                                      <span className="card-grid-staus-time">
                                        {moment(data.created).fromNow()}
                                      </span>
                                      <span className="card-grid-status card-grid-status-ri">
                                        {data.processDefinitionName
                                          ? data.processDefinitionName
                                          : data.caseDefinitionName}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </label>
                        </div>
                      );
                    })}
                  </div>
                </InfiniteScroll>
              ) : (
                <div className="container-fluid">
                  Hi {this.props.user.info.username}. No Tasks Found For You
                </div>
              )}
            </div>
          </div>
        </div>
        {/* <TaskDrawers /> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui
  };
};

export default connect(
  mapStateToProps,
  { getTasks, resetTask, selectedClaimTasks }
)(UnClaimedTask);
