import React, { Component } from 'react';
import { TaskUtility } from "../sharedTaskComponents";
import { getTasks, resetTask } from "../../../stateManager/actions";
import { taskConstant } from "../../../constants";
import { connect } from "react-redux";
import type from "../../../stateManager/types";

class GroupTask extends Component {
    state = {}
    async componentDidMount() {
        try {
            /** 
             *  trigger myTask api with auth token 
             **/
            this.setState({
                loading: true
            })
            // user, authToken, filter = null, offset = 10
            await this.props.getTasks({
                taskName: taskConstant.myGroupTasks,
                user: this.props.user.info.username,
                authToken: `Bearer ${this.props.user.info.authToken}`,
                filter: this.props.task.groupTask.filter,
                resultPointer: taskConstant.paginationStartingPointer,
                offset: taskConstant.paginationOffsetLimit,
                type: type.GET_GROUP_TASKS,
            })
            this.setState({
                loading: false
            })
        } catch (error) {
            this.setState({
                isError: true
            })
            console.error(error);
        } finally {
            this.setState({
                loading: false
            })
        }

    }

    componentWillUnmount(){
        this.props.resetTask(type.RESET_GROUP_TASKS)
    }

    render() {
        return (
            <React.Fragment>
                <TaskUtility taskName="Group Tasks" />
                <div className={"container-fluid"}>
                    {/* <h1>Group Tasks</h1> */}
                </div>
            </React.Fragment>
        );
    }
}


const mapStateToProps = (state) => {
    console.log('this is from myGroupTask');
    console.log(state);
    return {
        user: state.user,
        task: state.task,
        ui: state.ui
    }
}


export default connect(mapStateToProps, { getTasks, resetTask })(GroupTask);