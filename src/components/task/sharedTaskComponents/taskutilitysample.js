import React, { Component } from 'react';
import TaskFilter from "./TaskFilter";
class TaskUtility extends Component {
    state = {}
    render() {
        return (
            <div className="page-head-alt">
                <div className="page-head-top">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xs-12 col-lg-7">
                                <h1>{this.props.taskName}</h1>
                            </div>
                            {this.props.taskName === "My Tasks" &&
                                <div className="col-xs-12 col-lg-5 page-head-right">
                                    <ul className="list-inline activity-list">
                                        <li>
                                            <a className="alt" href="/task"><span data-toggle="tooltip" data-placement="bottom" title=""><i className="icon-time"></i></span></a>
                                        </li>
                                        <li>
                                            <a className="alt" href="/task"><span data-toggle="tooltip" data-placement="bottom" title=""><i className="icon-calendar"></i></span></a>
                                        </li>
                                        <li>
                                            <a className="alt" href="/task"><span data-toggle="tooltip" data-placement="bottom" title=""><i className="icon-users"></i></span></a>
                                        </li>
                                        <li>
                                            <a className="alt" href="/task"><span data-toggle="tooltip" data-placement="bottom" title=""><i className="icon-f-chat"></i></span></a>
                                        </li>
                                    </ul>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                {this.props.taskName !== "Unclaimed Tasks" && <TaskFilter />}
            </div>
        );
    }
}

export default TaskUtility;