import React, { Component } from "react";
// import { Select } from "antd";
// import Style from "./TaskFilter.module.css";
// const Option = Select.Option;

class TaskFilter extends Component {
  state = {};
  handleChange(value) {
    console.log(`selected ${value}`);
  }
  render() {
    return (
      <div className="page-head-bottom">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12 col-lg-8 hidden-xs">
              <div className="page-filter-item">
                <ul className="list-inline">
                  <li>
                    <select
                      className="form-control form-control-sm form-control-multiple"
                      multiple=""
                    >
                      <option>Status 22 </option>
                      <option>Status 1</option>
                      <option>Status 2</option>
                    </select>
                  </li>
                  <li>
                    <div className="dropdown dropdown-sm">
                      <button
                        className="dropdown-label"
                        id="invoice-amount"
                        data-toggle="dropdown"
                      >
                        Invoice Amount
                        <i className="icon-angle-down" />
                      </button>
                      <div
                        className="dropdown-menu invoice-slider"
                        aria-labelledby="invoice-amount"
                      >
                        <label>Invoice Amount</label>
                        <input
                          className="range-slider"
                          type="hidden"
                          value="14500,65000"
                        />
                        <span className="range-slider-value range-slider-value-in">
                          &#8377; <span>14000</span>
                        </span>
                        <span className="range-slider-value range-slider-value-fi">
                          &#8377; <span>65000</span>
                        </span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <select className="form-control form-control-sm">
                      <option>Date</option>
                      <option>Date 1</option>
                      <option>Date 2</option>
                    </select>
                  </li>
                  <li>
                    <a href="##" className="filter-btn">
                      More Filters
                    </a>
                  </li>
                  <li>
                    <label className="and-or-switch" htmlFor="filter-and-or">
                      <input
                        type="checkbox"
                        name="and-or-switch"
                        id="filter-and-or"
                      />
                      <span className="and-or-switch-in">
                        <span>And</span>
                        <span>Or</span>
                      </span>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xs-12 col-lg-4 hidden-xs">
              <div className="page-filter-item page-filter-item-right">
                <ul className="list-inline">
                  <li>
                    <div className="page-filter-item-search">
                      <i className="icon-search" />
                      <input
                        type="text"
                        className="form-control form-control-sm"
                        placeholder="Quick Find"
                      />
                    </div>
                  </li>
                  <li>
                    <select className="form-control form-control-sm">
                      <option>Sort by</option>
                      <option>Sort by 1</option>
                      <option>Sort by 2</option>
                    </select>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xs-6 visible-xs dropdown-sm">
              <a href="##" className="dropdown-label mobile-filter-tgr">
                Filters
              </a>
            </div>
            <div className="col-xs-6 visible-xs">
              <select className="form-control form-control-sm">
                <option>Sort by</option>
                <option>Sort by 1</option>
                <option>Sort by 2</option>
              </select>
            </div>
          </div>
        </div>
        <div className="mobile-filter-main visible-xs">
          <a href="##" className="filter-close">
            <i className="icon-close" />
          </a>
          <h2>Filters</h2>
          <div className="form-group">
            <select
              className="form-control form-control-sm form-control-multiple"
              multiple=""
            >
              <option>Status</option>
              <option>Status 1</option>
              <option>Status 2</option>
            </select>
          </div>
          <div className="form-group">
            <div className="dropdown dropdown-sm">
              <button
                className="dropdown-label"
                id="invoice-amount"
                data-toggle="dropdown"
              >
                Invoice Amount
                <i className="icon-angle-down" />
              </button>
              <div
                className="dropdown-menu invoice-slider"
                aria-labelledby="invoice-amount"
              >
                <label>Invoice Amount</label>
                <input
                  className="range-slider"
                  type="hidden"
                  value="14500,65000"
                />
                <span className="range-slider-value range-slider-value-in">
                  &#8377; <span>14000</span>
                </span>
                <span className="range-slider-value range-slider-value-fi">
                  &#8377; <span>65000</span>
                </span>
              </div>
            </div>
          </div>
          <div className="form-group">
            <select className="form-control form-control-sm">
              <option>Date</option>
              <option>Date 1</option>
              <option>Date 2</option>
            </select>
          </div>
          <div className="form-group">
            <label className="and-or-switch" htmlFor="filter-and-or-2">
              <input
                type="checkbox"
                name="and-or-switch"
                id="filter-and-or-2"
              />
              <span className="and-or-switch-in">
                <span>And</span>
                <span>Or</span>
              </span>
            </label>
          </div>
          <button type="" className="button button-primary button-sm">
            FILTER
          </button>
        </div>
      </div>
    );
  }
}

export default TaskFilter;
