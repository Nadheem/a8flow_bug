import React, { Component } from 'react';
import { connect } from "react-redux";
import className from "classnames";
import logo from "../../../assets/images/logo.svg";
import { Link } from "react-router-dom";
import { rootSelectedTab } from "../../../stateManager/actions";
class TaskNav extends Component {
    state = {}

    handleTab = (key) => {
        this.props.rootSelectedTab(key);
    }

    render() {
        return (
            <div className="page-head">
                <div className="page-head-main">
                    <img src={logo} alt="ESAF" className="site-logo" />
                    <div className="container-fluid">
                        <nav className="tab-list">
                            <ul className={true ? "dfdfds" : "dfd"}>
                                <li className={className({ "active": this.props.ui.activeRootTab === "myTask" })} onClick={this.handleTab.bind(this, "myTask")}>
                                    <Link to={`/task/myTask`}>My Tasks<span>{this.props.task.myTask.count}</span></Link>
                                </li>
                                <li className={className({ "active": this.props.ui.activeRootTab === "groupTasks" })} onClick={this.handleTab.bind(this, "groupTasks")} >
                                    <Link to={`/task/groupTasks/`}>Group Tasks<span>{this.props.task.groupTask.count}</span></Link>
                                </li>
                                <li className={className({ "active": this.props.ui.activeRootTab === "unclaimedTasks" })} onClick={this.handleTab.bind(this, "unclaimedTasks")}>
                                    <Link to={`/task/unclaimedTasks`}>Unclaimed Tasks<span>{this.props.task.unClaimedTask.count}</span></Link>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        state: state.user,
        ui: state.ui,
        task : state.task
    }
}
export default connect(mapStateToProps, { rootSelectedTab })(TaskNav);