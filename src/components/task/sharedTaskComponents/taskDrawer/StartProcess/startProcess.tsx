import * as React from "react";
import { Drawer, List, Button, Input, Icon } from "antd";
import {
  Axiosplus,
  TextBox,
  Validate,
  SelectHelper
} from "../../../../../helpers";
import { connect } from "react-redux";
import { getTasks } from "../../../../../stateManager/actions";
import { taskConstant } from "../../../../../constants";
import type from "../../../../../stateManager/types";
import "./StartProcess.css";
import VariableComponent from "./variableComponent";

//import "./TaskDrawer.css";

type Props = {
  startDrawerOnClose: (action: boolean) => void;
  startDrawerVisibility: boolean;
  getTasks: any;
  user: any;
  task: any;
  handleFormState : any
};

type StateProps = {
  processDefinitions: any[];
  drawerVisibility: boolean;
  startBtnVisibility: boolean;
  selectedProcessDefinitionId: string;
  formDetails: any;
};

class StartProcess extends React.Component<Props, StateProps> {
  state = {
    processDefinitions: [],
    drawerVisibility: false,
    startBtnVisibility: false,
    selectedProcessDefinitionId: "",
    formDetails: null
  };

  componentDidMount = async () => {
    // this.setState({
    //   drawerVisibility: this.props.startDrawerVisibility
    // });
    try {
      let result = await Axiosplus.get({
        path: `process-definition/?latest=true&active=true&startableInTasklist=true
            &startablePermissionCheck=true`,
        config: {
          headers: { Authorization: `Bearer ${this.props.user.info.authToken}` }
        }
      });
      this.setState({
        processDefinitions: result,
        drawerVisibility: this.props.startDrawerVisibility
      });
    } catch (error) {
      throw error;
    }
  };

  handleDrawerVisibility = (action: boolean) => {
    this.setState({
      drawerVisibility: action
    });
  };

  handleProcessDefinition = async (pId: string) => {
    try {
      this.setState({
        startBtnVisibility: true
      });

      let result = await Axiosplus.post({
        path: `/process-definition/${pId}/submit-form`,
        values: { variables: {} },
        config: {
          headers: { Authorization: `Bearer ${this.props.user.info.authToken}` }
        }
      });

      if (result) {
        await this.props.getTasks({
          taskName: taskConstant.myTask,
          user: this.props.user.info.username,
          authToken: `Bearer ${this.props.user.info.authToken}`,
          filter: this.props.task.myTask.filter,
          resultPointer: taskConstant.paginationStartingPointer,
          offset: taskConstant.paginationOffsetLimit,
          type: type.GET_MY_TASKS
        });
        this.setState({
          startBtnVisibility: false
        });
        this.handleDrawerVisibility(false);
      }
    } catch (error) {}
    console.log(pId);
  };

  startForm = async () => {
    try {
      let result = await Axiosplus.get({
        path: `/process-definition/${
          this.state.selectedProcessDefinitionId
        }/startForm`,
        config: {
          headers: { Authorization: `Bearer ${this.props.user.info.authToken}` }
        }
      });
      this.setState({ formDetails: result });
    } catch (error) {
      throw error;
    }
  };

  handleFormState = (state : null)=>{
    this.setState({
      formDetails : state
    })
  }

  processSubmitData = (values: any) => {
    console.log(JSON.parse(JSON.stringify(values)));
  };

  render() {
    return (
      <Drawer
        onClose={this.handleDrawerVisibility.bind(this, false)}
        visible={this.state.drawerVisibility}
        width={"400px"}
        destroyOnClose={true}
        afterVisibleChange={(v: any) => {
          if (v === false) {
            this.props.startDrawerOnClose(false);
          }
        }}
      >
        <React.Fragment>
          <div
            id="start-process"
            className="card-item-details card-item-add-new"
            style={{ transform: "translateX(0%)" }}
          >
            {!this.state.formDetails && (
              <div className="start-process-item start-pocess-item-start open">
                <div className="card-item-details-head">
                  <div className="card-item-details-head-top">
                    <h2 className="card-item-details-head-title">
                      <i
                        className="icon-left-arrow"
                        onClick={this.props.startDrawerOnClose.bind(
                          this,
                          false
                        )}
                      />
                      Search
                    </h2>
                  </div>
                </div>
                <div
                  className="card-item-details-content"
                  style={{ height: "386px" }}
                >
                  <div className="card-item-details-content-in">
                    <div className="form-group">
                      <div className="">
                        <Input
                          placeholder="Quick Find"
                          prefix={<i className="icon-search" />}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>CLICK ON ONE OF THE ITEMS TO PROCEED</label>
                      <ul className="list-unstyled check-list">
                        {this.state.processDefinitions.length > 0 &&
                          this.state.processDefinitions.map(
                            (processdefinition: any) => {
                              return (
                                <li key={processdefinition.id}>
                                  <label>
                                    <span className="input-radio input-control">
                                      <input
                                        type="radio"
                                        name="start-process-1"
                                        data-item="start-pocess-item-1"
                                        checked={
                                          this.state
                                            .selectedProcessDefinitionId ===
                                          processdefinition.id
                                        }
                                        onChange={() => {
                                          this.setState({
                                            selectedProcessDefinitionId:
                                              processdefinition.id
                                          });
                                        }}
                                      />
                                      <span>{processdefinition.name}</span>
                                    </span>
                                  </label>
                                </li>
                              );
                            }
                          )}
                      </ul>
                    </div>
                  </div>
                </div>

                <div className="start-process-item-btn">
                  <ul className="list-inline">
                    <li>
                      <button
                        className="button button-md button-primary start-process-item-button"
                        disabled={this.state.selectedProcessDefinitionId === ""}
                        onClick={this.startForm}
                      >
                        Proceed
                      </button>
                    </li>
                    <li>
                      <button
                        className="button button-md button-primary button-primary-alt popup-close"
                        onClick={this.props.startDrawerOnClose.bind(
                          this,
                          false
                        )}
                      >
                        Cancel
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            )}

            {this.state.formDetails && (
              <div
                id="start-pocess-item-1"
                className="start-process-item start-pocess-item-1"
              >
                <div className="card-item-details-head">
                  <div className="card-item-details-head-top">
                    <h2 className="card-item-details-head-title">
                      <i
                        className="icon-left-arrow"
                        onClick={this.props.startDrawerOnClose.bind(
                          this,
                          false
                        )}
                      />
                      DSA Form
                    </h2>
                  </div>
                </div>
                <VariableComponent handleFormState={this.handleFormState} />
              </div>
            )}
          </div>
        </React.Fragment>
      </Drawer>
    );
  }
}



const mapStateToProps = (state: any) => {
  return {
    user: state.user,
    ui: state.ui,
    task: state.task
  };
};

export default connect(
  mapStateToProps,
  { getTasks }
)(StartProcess);
