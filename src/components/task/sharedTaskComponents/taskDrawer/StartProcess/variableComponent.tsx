import * as React from "react";
import { Field, FieldArray, InjectedFormProps, reduxForm } from "redux-form";
import { TextBox, SelectHelper } from "../../../../../helpers";
import { Select, Button, Card } from "antd";
import "./StartProcess.css";

type props = {} & InjectedFormProps;

const validate = (values: any) => {
  let errors: any = {};
  if (values.variables && values.variables.length > 0) {
    let variablesArrayErrors: any = [];
    values.variables.forEach((variable: any, variableIndex: number) => {
      let variableErrors: any = {};
      if (!variable || !variable.variantName) {
        variableErrors.variantName = "Please enter name!";
      }
      if (!variable || !variable.variantType) {
        variableErrors.variantType = "Please select type!";
      }
      if (!variable || !variable.variantValue) {
        variableErrors.variantValue = "Please enter value!";
      }
      variablesArrayErrors[variableIndex] = variableErrors;
    });
    if (variablesArrayErrors.length) {
      errors.variables = variablesArrayErrors;
    }
  }
  return errors;
};

const variableDetails = ({ fields, meta: { touched, error } }: any) => {
  return (
    <div>
      <div className="form-group">
        <a
          href="#"
          className="add-variant"
          data-rel="#add-variant-content"
          onClick={() => fields.unshift({})}
        >
          <i className="icon-add" /> Add a variant
        </a>
      </div>
      {fields.map((field: any, index: any) => {
        return (
          <Card key={index} id="variablesCard">
            <div>
              <ul className="list-inline">
                <li>
                  <p>{`Variable #${fields.length - index}`}</p>
                </li>
                <li style={{ float: "right" }}>
                  <Button
                    type="danger"
                    title="Remove variant"
                    className="button-primary"
                    style={{ minWidth: "0px", height: "28px" }}
                    onClick={() => fields.remove(index)}
                  >
                    <i className="icon-bin" />
                  </Button>
                </li>
              </ul>
              <div className="form-group">
                <Field
                  label="Name"
                  component={TextBox}
                  name={`${field}.variantName`}
                  type="text"
                  hasFeedback
                  className="form-control-custom"
                  formItemLayout={{
                    labelCol: { span: 5 },
                    wrapperCol: { span: 17 }
                  }}
                />
              </div>
              <div className="form-group">
                <Field
                  label="Type"
                  component={SelectHelper}
                  name={`${field}.variantType`}
                  type="text"
                  hasFeedback
                  className="form-control-custom"
                  formItemLayout={{
                    labelCol: { span: 5 },
                    wrapperCol: { span: 17 }
                  }}
                >
                  <Select.Option value="boolean">Boolean</Select.Option>
                  <Select.Option value="short">Short</Select.Option>
                  <Select.Option value="integer">Integer</Select.Option>
                  <Select.Option value="long">Long</Select.Option>
                  <Select.Option value="double">Double</Select.Option>
                  <Select.Option value="string">String</Select.Option>
                  <Select.Option value="date">Date</Select.Option>
                </Field>
              </div>
              <div className="form-group">
                <Field
                  label="Value"
                  component={TextBox}
                  name={`${field}.variantValue`}
                  type="text"
                  hasFeedback
                  className="form-control-custom"
                  formItemLayout={{
                    labelCol: { span: 5 },
                    wrapperCol: { span: 17 }
                  }}
                />
              </div>
            </div>
          </Card>
        );
      })}
    </div>
  );
};

type VariableProps = {
  handleFormState: any;
} & InjectedFormProps;

class VariableComponent extends React.Component<any, any> {
  state = {};

  processSubmitData = (values: any) => {
    console.log(JSON.parse(JSON.stringify(values)));
  };

  render() {

    // const VariableComponent = (props:any) => {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    return (
      <form onSubmit={handleSubmit(this.processSubmitData)}>
        <div className="card-item-details-content" style={{ height: "500px" }}>
          <div className="card-item-details-content-in">
            <p>
              You can set variables, using a generic form, by clicking the "Add
              a variable" link below.
            </p>
            <div className="form-group">
              <label>Business Key</label>
              <input
                type="text"
                className="form-control"
                value="PSACE-5342"
                onChange={() => null}
              />
            </div>

            <FieldArray name="variables" component={variableDetails} />

            <div className="add-variant-main" />
          </div>
        </div>
        <div className="start-process-item-btn">
          <ul className="list-inline">
            <li>
              <button className="button button-md button-primary" type="submit">
                Start
              </button>
            </li>
            <li>
              <button
                className="button button-md button-primary button-primary-alt start-process-back"
                onClick={this.props.handleFormState()}
              >
                Back
              </button>
            </li>
          </ul>
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: "VariableComponent",
  validate
})(VariableComponent);
