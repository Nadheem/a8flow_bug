import React from "react";
import { connect } from "react-redux";
import Config from "../../../../constants";
import {
  addIframeReference,
  addTabInfo
} from "../../../../stateManager/actions";
import Validate from "validate.js";
import IframeLoader from "./formLoader"; 

class A8FlowHeart extends React.Component {
  constructor(props) {
    super(props);
    this.iFramePointer = React.createRef();
  }

  iFrameListener = e => {
    //extract the info what we need
    let { origin, data } = e;
    if (
      origin === "http://localhost:3001" ||
      origin === "https://storage.googleapis.com"
    ) {
      if (data.action === "sendBasicData") {
        let { info, name } = this.props.iframe.taskInfo;
        let tempTaskFound = this.props.task.tempTask[info.id];
        //if selected task found tempTask update temp data to task taskVariables
        if (!Validate.isEmpty(tempTaskFound)) {
          info = {
            ...info,
            taskVariables: { ...info.taskVariables, ...tempTaskFound }
          };
        }
        let data = {
          taskInfo: { info, name }
        };
        this.iFramePointer &&
          this.iFramePointer.contentWindow &&
          this.iFramePointer.contentWindow.postMessage(
            data,
            Config.TargetOrigin
          );
      }
    }
  };

  handleIframeOnLoad = () => {
    console.log("IFRAME ONLOAD IS TRIGGERED");
    let data = {
      init: true
    };
    this.iFramePointer.contentWindow.postMessage(data, Config.TargetOrigin);
    this.props.addIframeReference(this.iFramePointer);
    //listening data from iframe
    window.addEventListener("message", this.iFrameListener);
  };

  componentWillUnmount() {
    //remove the listener when cdm destroy
    window.removeEventListener("message", this.iFrameListener);
  }

  render() {
    return (
      <React.Fragment>
        {this.props.iframe.iframeLoader && (
          <IframeLoader />
        )}
        <div>
          <iframe
            width="100%"
            height="800px"
            title="test"
            src={this.props.iframe.iframeUrl}
            // src={"http://localhost:3001"}
            //  src="http://esaf.autonom8.com/apps/form1"
            frameBorder="0"
            style={{ background: "#fff", maxHeight: "1000px" }}
            ref={f => (this.iFramePointer = f)}
            onLoad={this.handleIframeOnLoad}
          />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui,
    iframe: state.iframe
  };
};

export default connect(
  mapStateToProps,
  { addIframeReference, addTabInfo }
)(A8FlowHeart);
