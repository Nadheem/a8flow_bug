import React, { Component } from "react";
import { connect } from "react-redux";
import StartProcess from "./StartProcess/startProcess";
import TaskForm from "./TaskForm";
import "./TaskDrawer.css";
class TaskDrawers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDrawerVisibility: false
    };
  }

  handleStartDrawer = action => {
    this.setState({
      startDrawerVisibility: action
    });
  };

  render() {
    return (
      <React.Fragment>
        {/*Task Form */}
        {this.props.task.taskFormVisibility && <TaskForm />}

        {/* </Drawer> */}
        {/* Start Process Code Start here */}
        {this.state.startDrawerVisibility && (
          <StartProcess
            startDrawerOnClose={this.handleStartDrawer}
            startDrawerVisibility={this.state.startDrawerVisibility}
          />
        )}
        {/* Start Process Code End here*/}

        <div className="add-new-task-main">
          <a href="#add" className="add-new-task">
            <i className="icon-add" />
          </a>
          <ul className="list-unstyled">
            <li>
              <a href="#card-item-add-new" className="popup-trigger">
                <i className="icon-task" />
                <span>Add Task</span>
              </a>
            </li>
            <li>
              {/* <a href="#start-process" className="popup-trigger" onClick={this.handleStartDrawer.bind(this,true)}> */}
              <a
                href="/"
                onClick={e => {
                  e.preventDefault();
                  this.handleStartDrawer(true);
                }}
              >
                <i className="icon-process" />
                <span>Start a process</span>
              </a>
            </li>
            <li>
              <a href="#add-group" className="popup-trigger">
                <i className="icon-group" />
                <span>Create a group</span>
              </a>
            </li>
            <li>
              <a href="#add-group" className="popup-trigger">
                <i className="icon-tenant" />
                <span>Create a tenant</span>
              </a>
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui,
    iframe: state.iframe
  };
};

export default connect(
  mapStateToProps,
  {}
)(TaskDrawers);
