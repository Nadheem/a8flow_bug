import React, { Component } from "react";
import { Button, notification, message, Drawer, Popover } from "antd";
import className from "classnames";
import { connect } from "react-redux";
import DetailsTab from "./detailsTab";
// import LeadTab from "./leadTab";
import IframeLoader from "./formLoader";
import Config from "../../../../constants";
import {
  updateTask,
  resetSelectedTask,
  getTasks,
  selectedTask,
  createComments,
  setActiveTab,
  completeTask,
  saveTask,
  addTabInfo,
  handleIframeLoader,
  handleTaskFromVisibility,
  removeIframeConfig
} from "../../../../stateManager/actions";
// import momentTz from "moment-timezone";
import IFrame from "./IFrame";
import Validate from "validate.js";
import CustomDatepicker from "../../../common/CustomDatepicker";
import Comments from "./Comments";
import AddGroups from "./AddGroups";

class TaskForm extends Component {
  state = {
    activeTab: "lead",
    tabInfo: {},
    primaryTabVisibility: 5,
    onCompleteLoader: false,
    tabLoader: true,
    startDrawerVisibility: false,
    drawerVisibility: false,
    loadIframe: false,
    triggerOnce: true,
    followUpDatePickerOpen: false,
    dueDatePickerOpen: false,
    comments: ""
  };

  componentDidMount() {
    console.log("FROM TASK FORM IS TRIGGERED");
    this.setState({
      drawerVisibility: this.props.task.taskFormVisibility,
      triggerOnce: true
    });
    //add postMessage listener
    window.addEventListener("message", this.ipcListener);
  }

  startComplete = () => {
    try {
      if (this.props.iframe.pointer) {
        this.props.iframe.pointer.contentWindow.postMessage(
          { formAction: "submit" },
          Config.TargetOrigin
        );
      } else {
        throw new Error("Error : a8flow iframe pointer not found.");
      }
    } catch (error) {
      throw error;
    }
  };

  startIPCAction = ({ action = null }) => {
    try {
      console.log("from startIPCAction function params");
      console.log(action);
      if (!action) {
        throw new Error("action props is missing @startIPCAction param");
      }
      if (this.props.iframe.pointer) {
        if (action === "submit") {
          this.props.iframe.pointer.contentWindow.postMessage(
            { formAction: action },
            Config.TargetOrigin
          );
        } else if (action === "save") {
          console.log("SAVE ACTION IS TRIGGERED");
          this.props.iframe.pointer.contentWindow.postMessage(
            { formAction: action },
            Config.TargetOrigin
          );
        }
      } else {
        throw new Error("Error : a8flow iframe pointer not found.");
      }
    } catch (error) {
      throw error;
    }
  };

  setActiveTab = data => {
    try {
      console.log(`CLICKED TAB : ${data}`);
      this.props.setActiveTab(data);
      console.log(this.props.iframe);
      if (this.props.iframe.pointer) {
        this.props.iframe.pointer.contentWindow.postMessage(
          { activeTab: data },
          // { activeTab: this.props.iframe.tabInfo.activeTab },
          Config.TargetOrigin
        );
      }
    } catch (error) {}
  };

  /**
   * ipcListener (Inter Process Communication via postmessage)
   * this function responsible for handling postmessage from external a8flowform app.
   */
  ipcListener = async e => {
    let { data, origin } = e;
    try {
      if (
        origin === "http://localhost:3001" ||
        origin === "https://storage.googleapis.com"
      ) {
        if (data.action && data.action === "onCompleteClick") {
          this.handleComplete(data);
        } else if (data.action && data.action === "onSaveClick") {
          this.handleSave(data);
        } else if (data.tabInfo) {
          //update iframe loader to false
          this.props.handleIframeLoader(false);
          this.props.addTabInfo(data.tabInfo);
          this.setState({
            tabLoader: false
          });
        }
      }
    } catch (error) {
      throw error;
    }
  };

  handleComplete = async data => {
    //extract task info and name for current iframe task
    let { info, name } = this.props.iframe.taskInfo;

    //trigger loading
    this.setState({
      onCompleteLoader: true
    });
    await this.props.completeTask({
      taskId: info.id,
      values: data.values,
      authToken: `Bearer ${this.props.user.info.authToken}`,
      taskName: name
    });
    //remove loading
    this.setState({
      onCompleteLoader: false
    });
    message.success("Task Submited Successfully.");
    // close the modal
    this.handleDrawerVisibility(false);
  };

  handleSave = data => {
    let { info, name } = this.props.iframe.taskInfo;
    console.log(this.props.iframe);
    console.log("FROM **** HANDLE SAVE");
    console.log(data);
    if (!Validate.isEmpty(data.values)) {
      this.props.saveTask({
        //values contain all current values what form has
        values: data.values,
        //task group name
        taskName: name,
        //task id
        taskId: info.id
      });
      this.openNotificationWithIcon(
        "success",
        "Form state saved successfully."
      );
    } else {
      this.openNotificationWithIcon("error", "Form is empty!");
    }
  };

  openNotificationWithIcon = (type, message) => {
    notification[type]({
      message: message,
      placement: "bottonRight",
      duration: 2
    });
  };

  updateTask = async (updateField, value) => {
    try {
      //trigger the update action to update the followUp Date
      let taskId = this.props.task.selectedTask.info.id;
      //below props for update the local task data after updated the task followUp
      // let taskArrayIndex = this.props.task.selectedTask.info.index;
      let authToken = `Bearer ${this.props.user.info.authToken}`;
      let {
        name,
        description,
        priority,
        assignee,
        owner,
        delegationState,
        due,
        followUp,
        parentTaskId,
        caseInstanceId,
        tenantId
      } = this.props.task.selectedTask.info;
      let values = {
        name,
        description,
        priority,
        assignee,
        owner,
        delegationState,
        due: updateField === "due" ? value : due,
        followUp: updateField === "followUp" ? value : followUp,
        parentTaskId,
        caseInstanceId,
        tenantId
      };
      await this.props.updateTask({
        taskId,
        values,
        authToken,
        taskName: this.props.task.selectedTask.name
      });
      // if (data) {
      this.setState({
        followUpDatePickerOpen: false,
        dueDatePickerOpen: false
      });
      // }
    } catch (error) {
      throw error;
    }
  };
/**
 * submit comments for each task.
 */
  submitComments = taskId => {
    this.props.createComments({
      taskId: taskId,
      values: { message: this.state.comments },
      authToken: `Bearer ${this.props.user.info.authToken}`
    });
  };

  componentWillUnmount() {
    //remove postMessage listener
    window.removeEventListener("message", this.ipcListener);
  }

  handleDrawerVisibility = action => {
    this.setState({
      drawerVisibility: action
    });
  };

  render() {
    let { info } = this.props.task.selectedTask;
    return (
      <Drawer
        onClose={() => {
          console.log("onClose is triggered");
          this.handleDrawerVisibility(false);
        }}
        closable={false}
        visible={this.state.drawerVisibility}
        width={"1000px"}
        maskClosable={false}
        // destroyOnClose={true}
        afterVisibleChange={v => {
          if (v === true) {
            if (this.state.triggerOnce) {
              console.log("@@@@@@@@@INSIDE TRUE IS TRIGGERED");
              this.setState({
                loadIframe: true,
                triggerOnce: false
              });
            }
          } else if (v === false) {
            console.log("INSIDE FALSE IS TRIGGERED");
            // this.props.startDrawerOnClose(false);
            this.props.handleTaskFromVisibility(false);
            this.props.removeIframeConfig();
            this.props.resetSelectedTask();
          }
        }}
      >
        <div className={"card-item-details-wrap"}>
          <div
            id="card-item-details-1"
            className={"card-item-details"}
            style={{ transform: "translateX(0%)", overflow: "auto" }}
          >
            <div className="card-item-details-head">
              <div className="row card-item-details-head-top">
                <div className="col-xs-7 col-md-4">
                  <h2 className="card-item-details-head-title">
                    <i
                      onClick={this.handleDrawerVisibility.bind(this, false)}
                      className="icon-left-arrow"
                    />
                    {info.name}
                  </h2>
                </div>
                <div className="col-xs-5 visible-xs visible-sm text-right">
                  <span className="card-grid-status card-grid-status-ri">
                    Renew Invoice
                  </span>
                </div>
                <div className="col-xs-12 col-md-8">
                  <ul className="list-inline activity-list">
                    <li className="hidden-xs hidden-sm">
                      <p>3 days ago</p>
                    </li>
                    <li>
                      <div
                        className="activity-list-item"
                        onClick={() =>
                          this.setState({ followUpDatePickerOpen: true })
                        }
                      >
                        <span
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Follow up date"
                        >
                          <i className="icon-time" />
                        </span>
                        {this.state.followUpDatePickerOpen && (
                          <CustomDatepicker
                            pickerOpen={this.state.followUpDatePickerOpen}
                            selectedDate={date =>
                              this.updateTask("followUp", date)
                            }
                            defaultDate={info.followUp}
                            onPickerOpenChange={() =>
                              this.setState({
                                followUpDatePickerOpen: !this.state
                                  .followUpDatePickerOpen
                              })
                            }
                          />
                        )}
                      </div>
                    </li>
                    <li>
                      <div
                        className="activity-list-item date-picker-trigger"
                        onClick={() =>
                          this.setState({ dueDatePickerOpen: true })
                        }
                      >
                        <span
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title="Due date"
                        >
                          <i className="icon-calendar" />
                        </span>
                        {this.state.dueDatePickerOpen && (
                          <CustomDatepicker
                            pickerOpen={this.state.dueDatePickerOpen}
                            selectedDate={date => this.updateTask("due", date)}
                            defaultDate={info.due}
                            onPickerOpenChange={() =>
                              this.setState({
                                dueDatePickerOpen: !this.state.dueDatePickerOpen
                              })
                            }
                          />
                        )}
                      </div>
                    </li>
                    <li>
                      <div className="dropdown">
                        <span
                          className="activity-list-item"
                          data-toggle="dropdown"
                        >
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Re-assign the task"
                          >
                            <i className="icon-users" />
                          </span>
                        </span>
                        <div className="dropdown-menu dropdown-menu-user pull-right">
                          <h2>Re-assign the task</h2>
                          <ul className="user-list">
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Michael Smith"
                                    />
                                    <span className="user-list-name">
                                      Michael Smith
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Amanda Thompson"
                                    />
                                    <span className="user-list-name">
                                      Amanda Thompson
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Samantha Garcia"
                                    />
                                    <span className="user-list-name">
                                      Samantha Garcia
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Christopher Johnson"
                                    />
                                    <span className="user-list-name">
                                      Christopher Johnson
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Sarah Marinez"
                                    />
                                    <span className="user-list-name">
                                      Sarah Marinez
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Joshua Jones"
                                    />
                                    <span className="user-list-name">
                                      Joshua Jones
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Jessica Harris"
                                    />
                                    <span className="user-list-name">
                                      Jessica Harris
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                            <li>
                              <label>
                                <span className="input-check input-control">
                                  <input type="checkbox" />
                                  <span>
                                    <img
                                      src="assets/images/temp/user-1.png"
                                      alt="Joseph Taylor"
                                    />
                                    <span className="user-list-name">
                                      Joseph Taylor
                                    </span>
                                  </span>
                                </span>
                              </label>
                            </li>
                          </ul>
                          <div className="drop-inner-wrap">
                            <button
                              type=""
                              className="button button-primary button-xsm"
                            >
                              REASSIGN
                            </button>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <Popover
                        content={<Comments />}
                        trigger={"click"}
                        placement={"bottomLeft"}
                      >
                        <span className="activity-list-item">
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Your comments"
                          >
                            <i className="icon-f-chat" />
                          </span>
                        </span>
                      </Popover>
                    </li>
                    <li>
                      <Popover
                        content={<AddGroups />}
                        trigger={"click"}
                        placement={"bottomLeft"}
                      >
                        <span className="activity-list-item">
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Add to groups"
                          >
                            <i className="icon-reasign" />
                          </span>
                        </span>
                      </Popover>
                    </li>
                    <li>
                      <div className="dropdown">
                        <span
                          className="activity-list-item"
                          data-toggle="dropdown"
                        >
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Task Description"
                          >
                            <i className="icon-info" />
                          </span>
                        </span>
                        <div className="dropdown-menu dropdown-menu-info pull-right">
                          <h2>Task Description</h2>
                          <div className="drop-inner-wrap">
                            <p>{info.description}</p>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="dropdown">
                        <span
                          className="activity-list-item"
                          data-toggle="dropdown"
                        >
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Task History"
                          >
                            <i className="icon-timeline" />
                          </span>
                        </span>
                        <div className="dropdown-menu dropdown-timeline pull-right">
                          <h2>Task History</h2>
                          <div className="task-history listi-unstyled">
                            <div className="task-history-main">
                              <img src="assets/images/temp/user-1.png" alt="" />
                              <div className="task-history-in">
                                <h3>Claim</h3>
                                <p>
                                  by You <span>24 Feb’19</span>
                                </p>
                              </div>
                            </div>
                            <div className="task-history-main">
                              <img src="assets/images/temp/user-1.png" alt="" />
                              <div className="task-history-in">
                                <h3>Loan Approved</h3>
                                <p>
                                  by Christopher Johnson <span>24 Feb’19</span>
                                </p>
                              </div>
                            </div>
                            <div className="task-history-main">
                              <img src="assets/images/temp/user-1.png" alt="" />
                              <div className="task-history-in">
                                <h3>Claim</h3>
                                <p>
                                  by You <span>24 Feb’19</span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li className="visible-xs visible-sm">
                      <p>3 days ago</p>
                    </li>
                  </ul>
                  <span className="card-grid-status card-grid-status-ri hidden-xs hidden-sm">
                    Renew Invoice
                  </span>
                </div>
              </div>
              <div className="row card-item-details-head-bottom">
                <div className="col-xs-12 col-md-9">
                  <nav className="tab-list">
                    <ul role="tablist">
                      <li
                        role="presentation"
                        className={className({
                          active: this.state.activeTab === "details"
                        })}
                        onClick={this.setActiveTab.bind(this, "details")}
                      >
                        <a href="#sdfs" role="tab" data-toggle="tab">
                          Details
                        </a>
                      </li>
                      {this.props.iframe.iframeLoader && (
                        <li>
                          <p style={{ color: "#CB1E1A" }}>
                            <b>Loading Tabs...</b>
                          </p>
                        </li>
                      )}

                      {!Validate.isEmpty(this.props.iframe.tabInfo) && (
                        <React.Fragment>
                          {this.props.iframe.tabInfo.tabList.length > 0 &&
                            this.props.iframe.tabInfo.tabList.map(
                              (data, index) => {
                                if (
                                  index <
                                  this.props.iframe.tabInfo.primaryTabVisibility
                                ) {
                                  return (
                                    <li
                                      key={index}
                                      role="presentation"
                                      className={className({
                                        active:
                                          this.props.iframe.tabInfo
                                            .activeTab === `${data.name}`
                                      })}
                                      onClick={this.setActiveTab.bind(
                                        this,
                                        `${data.name}`
                                      )}
                                    >
                                      <a
                                        href="#card-item-details-1-application"
                                        aria-controls="card-item-details-1-application"
                                        role="tab"
                                        data-toggle="tab"
                                      >
                                        {data.label}
                                      </a>
                                    </li>
                                  );
                                }
                                //default error thrown
                                return null;
                              }
                            )}

                          {this.props.iframe.tabInfo &&
                            this.props.iframe.tabInfo.tabList.length >
                              this.props.iframe.tabInfo
                                .primaryTabVisibility && (
                              <li role="presentation" className="dropdown">
                                <a href="#more" data-toggle="dropdown">
                                  More <i className="icon-angle-down" />
                                </a>
                                <ul className="dropdown-menu clk-fx">
                                  {this.props.iframe.tabInfo.tabList
                                    .slice(
                                      this.props.iframe.tabInfo
                                        .primaryTabVisibility
                                    )
                                    .map((data, index) => (
                                      <li key={index}>
                                        <a
                                          href="#card-item-details-1-lead-1"
                                          aria-controls="card-item-details-1-lead-2"
                                          role="tab"
                                          data-toggle="tab"
                                        >
                                          {data.name}
                                        </a>
                                      </li>
                                    ))}
                                </ul>
                              </li>
                            )}
                        </React.Fragment>
                      )}
                    </ul>
                  </nav>
                </div>
                <div className="col-xs-12 col-md-3 hidden-xs">
                  <ul className="list-inline ">
                    <li>
                      <Button
                        className="button button-primary button-xsm btnLoader"
                        loading={this.state.onCompleteLoader}
                        onClick={this.startIPCAction.bind(this, {
                          action: "submit"
                        })}
                      >
                        Complete
                      </Button>
                    </li>
                    <li>
                      <Button
                        className="button customFocus button-primary button-primary-alt button-xsm"
                        onClick={this.startIPCAction.bind(this, {
                          action: "save"
                        })}
                      >
                        Save
                      </Button>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="card-item-details-content">
              {this.props.iframe.tabInfo && (
                <div className="card-item-details-content-in">
                  <div
                    className={
                      this.props.iframe.tabInfo.activeTab === "details"
                        ? "activeTab"
                        : "inActiveTab"
                    }
                  >
                    <DetailsTab />
                  </div>
                  <div
                    className={
                      this.props.iframe.tabInfo.activeTab !== "details"
                        ? "activeTab"
                        : "inActiveTab"
                    }
                  >
                    {this.state.loadIframe ? (
                      <IFrame
                        activeTab={this.state.activeTab}
                        handleTabList={this.updateTabListFromIframe}
                      />
                    ) : (
                      <IframeLoader />
                    )}
                  </div>
                </div>
              )}
            </div>

            <div className="card-item-details-footer visible-xs">
              <Button
                className="button button-primary button-sm btnLoader"
                loading={this.state.onCompleteLoader}
                onClick={this.startIPCAction.bind(this, { action: "submit" })}
              >
                Complete
              </Button>
              <Button
                className="button button-primary button-primary-alt-2 button-sm"
                onClick={this.startIPCAction.bind(this, { action: "save" })}
              >
                Save
              </Button>
            </div>
          </div>
        </div>
      </Drawer>
    );
  }
}

const mapStateToProps = state => {
  console.log("from taskDrawer tab");
  console.log(state);
  return {
    user: state.user,
    task: state.task,
    ui: state.ui,
    iframe: state.iframe
  };
};

export default connect(
  mapStateToProps,
  {
    updateTask,
    resetSelectedTask,
    getTasks,
    selectedTask,
    createComments,
    setActiveTab,
    completeTask,
    saveTask,
    addTabInfo,
    handleIframeLoader,
    handleTaskFromVisibility,
    removeIframeConfig
  }
)(TaskForm);
