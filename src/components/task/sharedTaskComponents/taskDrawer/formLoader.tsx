import * as React from "react";
//@ts-ignore
import { Spin, Icon } from 'antd';

export default () => (
  <div
    style={{
      width: "100%",
      height: "100%",
      position: "absolute",
      background: "white"
    }}
  >
    <div
      style={{
        textAlign: "center",
        marginTop: "110px"
      }}
    >
      <div className="container-fluid">
        <div className="nothing-found" style={{color : "#CB1E1A", fontWeight : "bold"}}>
          <div className="nothing-found-icon">
            <Icon type="loading" style={{ fontSize: "43px", color : "#CB1E1A" }} spin />
          </div>
          <p style={{marginTop : "9px"}}>Processing Form...</p>
        </div>
      </div>
    </div>
  </div>
);
