import * as React from "react";
import { connect } from "react-redux";
import { getGroups } from "../../../../stateManager/actions";
import { Checkbox } from "antd";
import validate from "validate.js";

type AddGroupsState = {
  selectedGroups: number[];
};

class AddGroups extends React.Component<any, AddGroupsState> {
  constructor(props: any) {
    super(props);
    this.state = {
      selectedGroups: []
    };
  }

  selectGroups = (groupId: number) => {
    let { selectedGroups } = this.state;
    selectedGroups.push(groupId);
  };

  async componentDidMount() {
    const data = await this.props.getGroups({
      authToken: `Bearer ${this.props.user.info.authToken}`
    });
  }

  render() {
    return (
      <div>
        <h2>Re-assign the task</h2>
        <ul className="user-list">{this.groupRows()}</ul>
        <div className="drop-inner-wrap">
          <button className="button button-primary button-xsm">REASSIGN</button>
        </div>
      </div>
    );
  }

  groupRows(): JSX.Element {
    console.log(this.props.task.groups);
    return (
      !validate.isEmpty(this.props.task.groups) &&
      this.props.task.groups && this.props.task.groups.map((group: any) => {
          <li key={group.id}>
            <label>
              <span className="input-check input-control">
                <Checkbox onChange={() => this.selectGroups(group.id)} />
                <span>
                  <span className="user-list-name">{group.name}</span>
                </span>
              </span>
            </label>
          </li>
      })
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui,
    iframe: state.iframe
  };
};

export default connect(
  mapStateToProps,
  { getGroups }
)(AddGroups);
