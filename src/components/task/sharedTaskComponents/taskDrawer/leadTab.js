import React, { Component } from 'react';

class leadTab extends Component {
    state = {}
    render() {
        return (
            <React.Fragment>
                <div className="form-section">
                    <div className="form-section-head clearfix on">
                        <h3>Upload documents</h3>
                        <span className="status-label status-label-success">Complated</span>
                    </div>
                    <div className="form-section-content" style={{ "display": "block" }}>
                        <div className="flex-row">
                            <div className="form-group col-xs-12">
                                <div className="form-files">
                                    <span className="form-files-input">
                                        <input type="file" multiple="" name="" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-section">
                    <div className="form-section-head clearfix">
                        <h3>Basic Information</h3>
                        <span className="status-label status-label-pending">Pending</span>
                    </div>
                    <div className="form-section-content">
                        <div className="flex-row">
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">First name</label>
                                <input type="text" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Middle name</label>
                                <input type="text" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Last name</label>
                                <input type="text" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Gender</label>
                                <select className="form-control">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Title</label>
                                <select className="form-control">
                                    <option value="">Mr</option>
                                    <option value="">Miss</option>
                                    <option value="">Ms.</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Father's name</label>
                                <input type="text" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Place of birth</label>
                                <select className="form-control">
                                    <option value="">Louisiana</option>
                                    <option value="">Location 1</option>
                                    <option value="">Location 2</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-md-4">
                                <label htmlFor="">date of birth</label>
                                <div className="form-date">
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">01</option>
                                            <option value="">02</option>
                                            <option value="">03</option>
                                        </select>
                                    </div>
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">Jan</option>
                                            <option value="">Feb</option>
                                            <option value="">March</option>
                                        </select>
                                    </div>
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">1980</option>
                                            <option value="">1981</option>
                                            <option value="">1982</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Nationality</label>
                                <select className="form-control">
                                    <option value="">American</option>
                                    <option value="">Location 1</option>
                                    <option value="">Location 2</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Personal email</label>
                                <input type="email" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Alternate email</label>
                                <input type="email" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Marital status</label>
                                <select className="form-control">
                                    <option value="">Married</option>
                                    <option value="">Single</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-12 col-md-4">
                                <label htmlFor="">Anniversary date</label>
                                <div className="form-date">
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">01</option>
                                            <option value="">02</option>
                                            <option value="">03</option>
                                        </select>
                                    </div>
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">Jan</option>
                                            <option value="">Feb</option>
                                            <option value="">March</option>
                                        </select>
                                    </div>
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">1980</option>
                                            <option value="">1981</option>
                                            <option value="">1982</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group col-xs-12 col-md-4">
                                <label htmlFor="">Contact number</label>
                                <div className="form-mobile">
                                    <div className="form-mobile-code">
                                        <select className="form-control">
                                            <option value="">+1</option>
                                            <option value="">+91</option>
                                        </select>
                                    </div>
                                    <input type="text" className="form-control" placeholder="" />
                                </div>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Blood group</label>
                                <select className="form-control">
                                    <option value="">O +ve</option>
                                    <option value="">O -ve</option>
                                    <option value="">A +ve</option>
                                    <option value="">A -ve</option>
                                    <option value="">B +ve</option>
                                    <option value="">B -ve</option>
                                    <option value="">AB +ve</option>
                                    <option value="">AB -ve</option>
                                </select>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Gender</label>
                                <ul className="list-inline form-list">
                                    <li>
                                        <label>
                                            <span className="input-check input-control">
                                                <input type="checkbox" />
                                                <span>Male</span>
                                            </span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <span className="input-check input-control">
                                                <input type="checkbox" />
                                                <span>Female</span>
                                            </span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Gender</label>
                                <ul className="list-inline form-list">
                                    <li>
                                        <label>
                                            <span className="input-radio">
                                                <input type="radio" name="gender" />
                                                <span>Male</span>
                                            </span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <span className="input-radio">
                                                <input type="radio" name="gender" />
                                                <span>Female</span>
                                            </span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-section">
                    <div className="form-section-head clearfix">
                        <h3>Borrower Information</h3>
                        <span className="status-label status-label-pending">Pending</span>
                    </div>
                    <div className="form-section-content">
                        <div className="flex-row">
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Relationship</label>
                                <select className="form-control form-error">
                                    <option value="">Father</option>
                                    <option value="">Mother</option>
                                    <option value="">Spouse</option>
                                    <option value="">Son</option>
                                    <option value="">Daughter</option>
                                </select>
                                <span className="form-error-message">This field is mandatory</span>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">First name</label>
                                <input type="text" className="form-control form-error" placeholder="" />
                                <span className="form-error-message">This field is mandatory</span>
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Last name</label>
                                <input type="text" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-6 col-md-4">
                                <label htmlFor="">Email</label>
                                <input type="email" className="form-control" placeholder="" />
                            </div>
                            <div className="form-group col-xs-12 col-md-4">
                                <label htmlFor="">date of birth</label>
                                <div className="form-date">
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">01</option>
                                            <option value="">02</option>
                                            <option value="">03</option>
                                        </select>
                                    </div>
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">Jan</option>
                                            <option value="">Feb</option>
                                            <option value="">March</option>
                                        </select>
                                    </div>
                                    <div className="form-date-part">
                                        <select className="form-control">
                                            <option value="">1980</option>
                                            <option value="">1981</option>
                                            <option value="">1982</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default leadTab;