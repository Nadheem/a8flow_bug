import * as React from "react";
import { Input, Button, Divider } from "antd";
import { connect } from "react-redux";
import { createComments } from "../../../../stateManager/actions";

export interface CommentsState {
  comments: string;
}

class Comments extends React.Component<any, CommentsState> {
  constructor(props: any) {
    super(props);
    this.state = { comments: "" };
  }

  submitComments = async () => {
    let data = await this.props.createComments({
      taskId: this.props.task.selectedTask.info.id,
      values: { message: this.state.comments },
      authToken: `Bearer ${this.props.user.info.authToken}`
    });
    console.log(data);
  };

  render() {
    return (
      <div>
        <h2>Write a Note</h2>
        <Divider />
        <div className="drop-inner-wrap">
          <div className="form-group">
            <label>Your comments</label>
            <Input.TextArea
              className="form-control"
              placeholder="Start typeing..."
              onChange={event => {
                this.setState({ comments: event.target.value });
              }}
              value={this.state.comments}
            />
          </div>
          <Button
            className="button button-primary button-xsm"
            block
            onClick={this.submitComments}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

// export default Comments;

const mapStateToProps = (state: any) => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui,
    iframe: state.iframe
  };
};

export default connect(
  mapStateToProps,
  {
    createComments
  }
)(Comments);
