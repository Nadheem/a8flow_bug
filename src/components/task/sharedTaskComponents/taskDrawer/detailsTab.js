import React, { Component } from 'react';
import { connect } from "react-redux";
class DetailsTab extends Component {
    state = {}
    render() {
        let { info } = this.props.task.selectedTask;
        return (
            <div className="tab-content">
                <div role="tabpanel" className="tab-pane active" id="card-item-details-1-details">
                    <div className="tab-content-main">
                        <div className="tab-content-in">
                            <h2>Task Summary</h2>
                            <div className="row">
                                {info._embedded && info._embedded.variable &&
                                    info._embedded.variable.map((v, index) => (
                                        <div key={index} className="col-xs-6 col-sm-4 col-md-3">
                                            <p className="text-label">{v.name}</p>
                                            <p><strong>{v.value}</strong></p>
                                        </div>
                                    ))}
                            </div>
                        </div>
                        <div className="tab-content-in">
                            <h2>Task Description</h2>
                            {info.description ?
                                <p>{info.description}</p>
                                : <p>No Description Found!</p>}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        task: state.task,
        ui: state.ui
    }
}

export default connect(mapStateToProps, {})(DetailsTab);