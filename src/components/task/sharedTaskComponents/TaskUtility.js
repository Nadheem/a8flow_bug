import React, { Component } from "react";
import { Button } from "antd";
import TaskFilter from "./TaskFilter";
import { connect } from "react-redux";
import {
  claimTask,
  getAllTaskCounts,
  removeClaimTask
} from "../../../stateManager/actions/task.action";
import Validate from "validate.js";
import { message } from "antd";

class TaskUtility extends Component {
  constructor(props) {
    super(props);
    this.state = {
      btnLoading: false
    };
  }
  //submit the unclaimed tasks that are selected to claim to the current user
  submitClaimTasks = async () => {
    let { selectedTasks } = this.props.task.unClaimedTask;
    if (Validate.isEmpty(selectedTasks)) {
      message.error("Please select some task and try again!");
      return;
    }
    let { username, authToken } = this.props.user.info;
    try {
      this.setState({
        btnLoading: true
      });
      let test = await Promise.all(
        selectedTasks.map(async task => {
          await this.props.claimTask({
            task,
            values: { userId: username },
            authToken: `Bearer ${authToken}`
          });
        })
      );
      if (test) {
        this.setState({ btnLoading: false });
        this.props.removeClaimTask();
        this.props.getAllTaskCounts({
          authToken: `Bearer ${authToken}`
        });
      }
    } catch (e) {
      this.setState({ btnLoading: false });
      throw e;
    }
  };

  render() {
    return (
      <div className="page-head">
        <div className="page-head-alt">
          <div className="page-head-top">
            <div className="container-fluid">
              <div className="row">
                <div className="col-xs-6 col-lg-7">
                  <h1>{this.props.taskName}</h1>
                </div>
                {this.props.taskName === "My Tasks" && (
                  <div className="col-xs-6 col-lg-5 page-head-right">
                    <ul className="list-inline activity-list hidden-xs">
                      <li>
                        <a className="activity-list-item" href="/test">
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Reschedule"
                          >
                            <i className="icon-time" />
                          </span>
                        </a>
                      </li>
                      <li>
                        <span className="activity-list-item date-picker-trigger">
                          <span
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Calendar"
                          >
                            <i className="icon-calendar" />
                          </span>
                          <input type="hidden" className="date-picker" />
                        </span>
                      </li>
                      <li>
                        <div className="dropdown">
                          <span
                            className="activity-list-item"
                            data-toggle="dropdown"
                          >
                            <span
                              data-toggle="tooltip"
                              data-placement="bottom"
                              title="Re-assign"
                            >
                              <i className="icon-users" />
                            </span>
                          </span>
                          <div className="dropdown-menu dropdown-menu-user pull-right">
                            <h2>Re-assign the task</h2>
                            <ul className="user-list">
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Michael Smith"
                                      />
                                      <span className="user-list-name">
                                        Michael Smith
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Amanda Thompson"
                                      />
                                      <span className="user-list-name">
                                        Amanda Thompson
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Samantha Garcia"
                                      />
                                      <span className="user-list-name">
                                        Samantha Garcia
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Christopher Johnson"
                                      />
                                      <span className="user-list-name">
                                        Christopher Johnson
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Sarah Marinez"
                                      />
                                      <span className="user-list-name">
                                        Sarah Marinez
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Joshua Jones"
                                      />
                                      <span className="user-list-name">
                                        Joshua Jones
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Jessica Harris"
                                      />
                                      <span className="user-list-name">
                                        Jessica Harris
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                              <li>
                                <label>
                                  <span className="input-check input-control">
                                    <input type="checkbox" />
                                    <span>
                                      <img
                                        src="assets/images/temp/user-1.png"
                                        alt="Joseph Taylor"
                                      />
                                      <span className="user-list-name">
                                        Joseph Taylor
                                      </span>
                                    </span>
                                  </span>
                                </label>
                              </li>
                            </ul>
                            <div className="drop-inner-wrap">
                              <button
                                type=""
                                className="button button-primary button-xsm"
                              >
                                REASSIGN
                              </button>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="dropdown">
                          <span
                            className="activity-list-item"
                            data-toggle="dropdown"
                          >
                            <span
                              data-toggle="tooltip"
                              data-placement="bottom"
                              title="Write a note"
                            >
                              <i className="icon-f-chat" />
                            </span>
                          </span>
                          <div className="dropdown-menu dropdown-menu-write-a-note pull-right">
                            <h2>Write a Note</h2>
                            <div className="drop-inner-wrap">
                              <div className="form-group">
                                <label>Your comments</label>
                                <textarea
                                  className="form-control"
                                  name=""
                                  placeholder="Start typeing..."
                                />
                              </div>
                              <button
                                type=""
                                className="button button-primary button-xsm"
                              >
                                Submit
                              </button>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <ul className="list-inline activity-list visible-xs">
                      <li>
                        <a
                          className="activity-list-item searchitem"
                          href="/test"
                        >
                          <i className="icon-search" />
                        </a>
                      </li>
                      <li>
                        <a
                          className="activity-list-item activity-list-trigger"
                          href="/test"
                        >
                          <span className="list-stcak" />
                        </a>
                        <ul className="list-unstyled">
                          <li>
                            <a className="activity-list-item" href="/test">
                              <span
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title=""
                              >
                                <i className="icon-time" />
                              </span>
                            </a>
                          </li>
                          <li>
                            <span className="activity-list-item date-picker-trigger">
                              <span
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title=""
                              >
                                <i className="icon-calendar" />
                              </span>
                              <input type="hidden" className="date-picker" />
                            </span>
                          </li>
                          <li>
                            <div className="dropdown">
                              <span
                                className="activity-list-item"
                                data-toggle="dropdown"
                              >
                                <span
                                  data-toggle="tooltip"
                                  data-placement="bottom"
                                  title=""
                                >
                                  <i className="icon-users" />
                                </span>
                              </span>
                              <div className="dropdown-menu dropdown-menu-user pull-right">
                                <h2>Re-assign the task</h2>
                                <ul className="user-list">
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Michael Smith"
                                          />
                                          <span className="user-list-name">
                                            Michael Smith
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Amanda Thompson"
                                          />
                                          <span className="user-list-name">
                                            Amanda Thompson
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Samantha Garcia"
                                          />
                                          <span className="user-list-name">
                                            Samantha Garcia
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Christopher Johnson"
                                          />
                                          <span className="user-list-name">
                                            Christopher Johnson
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Sarah Marinez"
                                          />
                                          <span className="user-list-name">
                                            Sarah Marinez
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Joshua Jones"
                                          />
                                          <span className="user-list-name">
                                            Joshua Jones
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Jessica Harris"
                                          />
                                          <span className="user-list-name">
                                            Jessica Harris
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                  <li>
                                    <label>
                                      <span className="input-check input-control">
                                        <input type="checkbox" />
                                        <span>
                                          <img
                                            src="assets/images/temp/user-1.png"
                                            alt="Joseph Taylor"
                                          />
                                          <span className="user-list-name">
                                            Joseph Taylor
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </li>
                                </ul>
                                <div className="drop-inner-wrap">
                                  <button
                                    type=""
                                    className="button button-primary button-xsm"
                                  >
                                    REASSIGN
                                  </button>
                                </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="dropdown">
                              <span
                                className="activity-list-item"
                                data-toggle="dropdown"
                              >
                                <span
                                  data-toggle="tooltip"
                                  data-placement="bottom"
                                  title=""
                                >
                                  <i className="icon-f-chat" />
                                </span>
                              </span>
                              <div className="dropdown-menu dropdown-menu-write-a-note pull-right">
                                <h2>Write a Note</h2>
                                <div className="drop-inner-wrap">
                                  <div className="form-group">
                                    <label>Your comments</label>
                                    <textarea
                                      className="form-control"
                                      name=""
                                      placeholder="Start typeing..."
                                    />
                                  </div>
                                  <button
                                    type=""
                                    className="button button-primary button-xsm"
                                  >
                                    Submit
                                  </button>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                )}

                {this.props.taskName === "Unclaimed Tasks" && (
                  <div className="col-xs-6 col-lg-5 page-head-right">
                    <Button
                      size="large"
                      className={"button button-primary button-sm claimTask"}
                      onClick={() => this.submitClaimTasks()}
                      loading={this.state.btnLoading}
                    >
                      Claim Task
                    </Button>
                  </div>
                )}
              </div>
              <div className="page-filter-item-search-main">
                <div className="page-filter-item-search">
                  <i className="icon-search" />
                  <input
                    type="text"
                    className="form-control form-control-sm"
                    placeholder="Quick Find"
                  />
                </div>
              </div>
            </div>
          </div>
          {this.props.taskName !== "Unclaimed Tasks" && <TaskFilter />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    task: state.task,
    ui: state.ui
  };
};

export default connect(
  mapStateToProps,
  { claimTask, getAllTaskCounts, removeClaimTask }
)(TaskUtility);
