import * as React from "react";
import { DatePicker } from "antd";
import { Moment } from "moment-timezone";
import moment from "moment";

export interface ICustomDatepickerProps {
  pickerOpen: boolean;
  selectedDate: (date: string) => void;
  onPickerOpenChange: () => void;
  defaultDate: string;
}

export interface ICustomDatepickerState {}

class CustomDatepicker extends React.Component<
  ICustomDatepickerProps,
  ICustomDatepickerState
> {
  constructor(props: ICustomDatepickerProps) {
    super(props);
  }

  render() {
    return (
      <div>
        <div id="datesize">
          <DatePicker
            className="datepicker-component"
            open={this.props.pickerOpen}
            onChange={(moment, dateString: string) =>
              this.props.selectedDate(dateString)
            }
            onOpenChange={() => this.props.onPickerOpenChange()}
            format="YYYY-MM-DDTHH:mm:ss.SSSZZ"
            defaultPickerValue={
              this.props.defaultDate
                ? moment(this.props.defaultDate)
                : undefined
            }
            disabledDate={(currentDate: any) => {
              return moment(currentDate) < moment(new Date());
            }}
            showToday={false}
          />
        </div>
      </div>
    );
  }
}

export default CustomDatepicker;
