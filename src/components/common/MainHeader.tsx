import * as React from "react";
import logo from "../../assets/images/logo.svg";
import { LeftNav, Footer } from "../../helpers";

export interface IMainHeaderProps {
}

export interface IMainHeaderState {}

class MainHeader extends React.Component<IMainHeaderProps, IMainHeaderState> {
  state = {};
  render() {
    return (
      <div id="page">
        <header className="site-header">
          <div className="container-fluid">
            <span className="nav-toggle">
              <span />
            </span>
            <img src={logo} alt="ESAF" />
          </div>
        </header>
        <div id="main" className="site-main" role="main">
          <LeftNav />
          <div className="page-wrap page-fixed-nav">
            
            {this.props.children}
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default MainHeader;
