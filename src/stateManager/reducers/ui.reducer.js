import type from "../types";

const initialState = {

    /**
     * This prop response for add active 
     * class to root nav current selection
     */
    activeRootNav: "task",

    /**
     * This props track the current Dashboard nav 
     */
    activeRootTab: "myTask"

};

export default (state = initialState, action) => {
    switch (action.type) {
        case type.ROOT_SELECTED_MENU:
        let initialRootTab =  action.data === "task" ? "myTask" : "profile"
            return {
                ...state,
                activeRootNav: action.data,
                activeRootTab: initialRootTab
            };
        case type.ROOT_SELECTED_TAB:
            return {
                ...state,
                activeRootTab:action.data
            };
        case type.SIGNOUT:
            return {
                ...initialState
            }
        default:
            return state;
    }
};
