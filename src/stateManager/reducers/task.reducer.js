import type from "../types";

const initialState = {
  myTask: {
    /**
     * count contains how many task tha user has
     */
    count: 0,
    /**
     * filter object contains the filter info
     * this object info is required to get myTaskList info
     */
    filter: {},
    /**
     * contains myTask list belongs to logged in user
     */
    taskList: []
  },
  groupTask: {
    /**
     * count contains how many group task tha user has
     */
    count: 0,
    /**
     * filter object contains the filter info
     * this object info is required to get groupList info
     */
    filter: {},
    /**
     * contains groupTask list belongs to logged in user
     */
    taskList: []
  },
  unClaimedTask: {
    /**
     * count contains how many unclaimed task tha user has
     */
    count: 0,
    /**
     * filter object contains the filter info
     * this object info is required to get unclaimedTask info
     */
    filter: {},
    /**
     * contains unClaimedTask list belongs to logged in user
     */
    taskList: [],
    /**
     * contains tasks that are selected to claim for the current logged-in user
     */
    selectedTasks: []
  },
  selectedTask: {
    //name responsible for intimate which task we are selecting
    name: null,
    //info contain all selected task info
    info: {}
  },
  tempTask: {},
  taskFormVisibility: false,
  groups: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case type.GET_MY_TASKS:
      let { count, filter, taskList } = action.data;
      return {
        ...state,
        myTask: {
          ...state.myTask,
          count,
          filter,
          taskList
        }
      };
    case type.FETCH_MORE_TASKS:
      let taskName =
        action.taskName === "All Tasks"
          ? "unClaimedTask"
          : action.taskName === "My Tasks"
          ? "myTask"
          : "groupTask";
      return {
        ...state,
        [taskName]: {
          ...state[taskName],
          // taskList : state.myTask.taskList.concat(action.data.taskList),
          taskList: [...state[taskName].taskList, ...action.data.taskList]
        }
      };
    case type.GET_GROUP_TASKS: {
      let { count, filter, taskList } = action.data;
      return {
        ...state,
        groupTask: {
          ...state.groupTask,
          count,
          filter,
          taskList
        }
      };
    }
    case type.GET_UNCLAIMED_TASKS: {
      let { count, filter, taskList } = action.data;
      return {
        ...state,
        unClaimedTask: {
          ...state.unclaimedTask,
          count,
          filter,
          taskList
        }
      };
    }
    case type.RESET_TASKS:
      return {
        ...state,
        [action.where]: {
          ...state[action.where],
          // count: 0,
          filter: {},
          taskList: []
        }
      };

    case type.INIT_TASK_COUNTS: {
      let { data } = action;
      return {
        ...state,
        myTask: {
          ...state.myTask,
          count: data.myTask ? data.myTask : 0
        },
        groupTask: {
          ...state.groupTask,
          count: data.groupTask ? data.groupTask : 0
        },
        unClaimedTask: {
          ...state.unClaimedTask,
          count: data.unClaimedTask ? data.unClaimedTask - data.myTask : 0
        }
      };
    }

    case type.SELECTED_TASK:
      return {
        ...state,
        selectedTask: {
          ...state.selectedTask,
          name: action.data.name,
          info: action.data.info
        }
      };

    case type.RESET_SELECTED_TASK:
      return {
        ...state,
        selectedTask: {
          ...state.selectedTask,
          ...initialState.selectedTask
        }
      };

    case type.UPDATE_TASK: {
      let { taskId, taskName, values } = action.data;
      let taskList = [...state[taskName].taskList].map(data => {
        if (data.id === taskId) {
          return { ...data, ...values };
        } else {
          return data;
        }
      });
      return {
        ...state,
        [taskName]: {
          ...state[taskName],
          taskList
        },
        selectedTask: {
          ...state.selectedTask,
          info: {
            ...state.selectedTask.info,
            ...values
          }
        }
      };
    }

    case type.COMPLETE_TASK: {
      let { taskId, taskName } = action.data;
      let taskList = [...state[taskName].taskList].filter(data => {
        if (data.id === taskId) {
          return null;
        } else {
          return data;
        }
      });

      //remove the taskId from tempTask if found
      let { tempTask } = state;
      if (tempTask[taskId]) {
        delete tempTask[taskId];
      }

      return {
        ...state,
        [taskName]: {
          ...state[taskName],
          taskList,
          //decrease the overall task count when complete action triggered.
          count: --state[taskName].count
        },
        tempTask
      };
    }

    case type.SAVE_TASK: {
      let { taskId, values } = action.data;
      let { tempTask } = state;
      if (tempTask[taskId]) {
        tempTask[taskId] = values;
      } else {
        tempTask[taskId] = values;
      }
      return {
        ...state,
        tempTask
      };
    }

    case type.TASK_FORM_VISIBILITY: {
      return {
        ...state,
        taskFormVisibility: action.data
      };
    }

    case type.SELECTED_CLAIM_TASKS: {
      return {
        ...state,
        unClaimedTask: {
          ...state.unClaimedTask,
          selectedTasks: action.data
        }
      };
    }

    case type.REMOVE_CLAIM_TASK: {
      let { selectedTasks, taskList, count } = state.unClaimedTask;
      let updatedTaskList = [];
      let selectedTasksIds = selectedTasks.map(x => x.id);
      taskList.forEach(task => {
        if (selectedTasksIds.indexOf(task.id) === -1) {
          updatedTaskList.push(task);
        }
      });
      return {
        ...state,
        unClaimedTask: {
          ...state.unClaimedTask,
          count: count - selectedTasks.length + 1,
          taskList: updatedTaskList,
          selectedTasks: []
        }
      };
    }

    case type.GET_GROUPS: {
      return {
        ...state,
        groups: state.groups, ...action.data
      };
    }

    default:
      return state;
  }
};
