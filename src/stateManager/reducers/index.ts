import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import userReducer from "./auth.reducer";
import uiReducer from "./ui.reducer";
import taskReducer from "./task.reducer";
import iframe from "./iframe.reducer";
//comine all subreducers 
const RootReducer = combineReducers({
  //user reducer contain all the 
  user : userReducer,
  ui : uiReducer,
  task : taskReducer,
  form,
  iframe
});


export type AppState = ReturnType<typeof RootReducer>;

export default RootReducer;


