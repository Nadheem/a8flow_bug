import type from "../types";

const initialState = {
  /**
   * Tab Info contain all tabConfiguration by user selected task
   */
  tabInfo: {
    activeTab: null,
    primaryTabVisibility: 5,
    tabList: []
  },

  /**
   * this will hold what user currently selected
   */
  taskInfo: {},

  //base url
  baseUrl: "https://storage.googleapis.com/asia.a8flowapps.autonom8.com",

  //taget url
  iframeUrl: "",
  /**
   * Reference to current IFrame Dom
   */
  pointer: null,

  //this prop responsible for handling initial iframeLoader
  iframeLoader: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case type.ADD_TAB_INFO:
      return {
        ...state,
        tabInfo: action.data
      };
    case type.ADD_POINTER:
      return {
        ...state,
        pointer: action.data
      };
    case type.ADD_SELECTED_TASK_TO_IFRAME: {
      return {
        ...state,
        taskInfo: action.data
      };
    }
    case type.SET_ACTIVE_TAB:
      return {
        ...state,
        tabInfo: {
          ...state.tabInfo,
          activeTab: action.data
        }
      };
    case type.ADD_IFRAME_URL:
      return {
        ...state,
        iframeUrl: action.data
      };
    case type.IFRAME_LOADER:
      return {
        ...state,
        iframeLoader: action.data
      };
    case type.CLEAR_IFRAME_CONFIG:
      return {
        ...initialState,
        pointer: state.pointer,
      };
    default:
      return state;
  }
};
