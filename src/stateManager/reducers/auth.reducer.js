import type from "../types";

const initialState = {
  
  /** login status
   *  type : boolean
   **/
  isLoggedIn: false,
  
  //maintain auth flow loading status 
  isLoading: false,

  //basic user info 
  info : null,
  
  //if any auth error
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case type.SIGNIN:
      return {
        ...state,
        isLoading: true
      };

    case type.SIGNIN_SUCCESS:
      console.log('from auth reducer');
      console.log(action.data);
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
        info: action.data,
        error: null
      };

    case type.SIGNIN_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error
      };

    case type.SIGNOUT:
      return {
        ...initialState
      };

    default:
      return state;
  }
};
