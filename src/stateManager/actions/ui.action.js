import type from "../types";

export const rootSelectedMenu = (data) =>{
    return {
        type : type.ROOT_SELECTED_MENU,
        data
    }
}

export const rootSelectedTab = (data) =>{
    return {
        type : type.ROOT_SELECTED_TAB,
        data
    }
}

