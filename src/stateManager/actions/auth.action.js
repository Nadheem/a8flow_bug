import type from "../types";
import { Axiosplus } from "../../helpers";


export const signSuccess = (data) => {
    console.log("signin action is triggered");
    return {
        type: type.SIGNIN_SUCCESS,
        data
    }
}

// export const signin = (data) => {
//     console.log("signin action is triggered");
//     return {
//         type: type.SIGNIN_SUCCESS,
//         data
//     }
// }


export const signin = (values) => {
    return async dispatch => {
        try {
            const data = await Axiosplus.post({ path: "/authentication/", values });
            dispatch(signSuccess(data));
            return data;
        } catch (e) {
            throw e;
        }
    };
}

export const signout = () => {
    return {
        type: type.SIGNOUT
    }
}