import type from "../types";
import { Axiosplus } from "../../helpers";
import { _schemaGetTask, ajv } from "../../helpers";
import { taskConstant } from "../../constants";
import Validate from "validate.js";
export const getTasks = taskParam => {
  return async dispatch => {
    try {
      //check expected parm available
      if (!ajv.validate(_schemaGetTask, taskParam)) {
        // throw new Error(ajv.errors);
        throw ajv.errors;
      } else {
        let filterdTask = await fillterTask(taskParam);
        let { count, filter, taskList } = filterdTask;
        dispatch({
          type: taskParam.type,
          taskName: taskParam.taskName,
          data: {
            count,
            filter,
            taskList
          }
        });
        return filterdTask;
      }
    } catch (e) {
      throw e;
    }
  };
};

export const getAllTaskCounts = ({ authToken }) => {
  return async dispatch => {
    try {
      const data = await Axiosplus.get({
        path: `filter?resourceType=Task&itemCount=true`,
        config: { headers: { Authorization: authToken } }
      });
      //setp2 -> filter the myTasks data form data response above
      let taskCountFilter = {};
      data.forEach(value => {
        if (value.name === taskConstant.myTask) {
          taskCountFilter["myTask"] = value.itemCount;
        } else if (value.name === taskConstant.myGroupTasks) {
          taskCountFilter["groupTask"] = value.itemCount;
        } else if (value.name === taskConstant.unClaimedTasks) {
          taskCountFilter["unClaimedTask"] = value.itemCount;
        }
      });
      dispatch({
        type: type.INIT_TASK_COUNTS,
        data: taskCountFilter
      });
    } catch (error) {
      throw error;
    }
  };
};

let fillterTask = async ({
  taskName = taskConstant.myTask,
  user,
  authToken,
  filter,
  resultPointer = 0,
  offset
}) => {
  try {
    let taskFilter = null;
    //if filter found reduce the api call to get the myTask id from filter
    if (!Validate.isEmpty(filter)) {
      taskFilter = filter;
    } else {
      //if filter not found in function params start the normal process
      //step1 -> get the MyTask id using filter endpoint
      const data = await Axiosplus.get({
        path: `filter?resourceType=Task&itemCount=true`,
        config: { headers: { Authorization: authToken } }
      });
      //setp2 -> filter the myTasks data form data response above
      taskFilter = data.find(value => value.name === taskName);
    }
    let httpMethod = "post";
    let httpPostData = {
      sortBy: "created",
      sortOrder: "desc"
    };
    let queryParamBuilder = `firstResult=${resultPointer}&maxResults=${offset}`;
    if (taskName === taskConstant.myTask) {
      httpPostData = { ...httpPostData, assignee: `${user}`, assigned: true };
    } else if (taskName === taskConstant.unClaimedTasks) {
      // httpMethod = "post";
      httpPostData = { ...httpPostData, unassigned: true };
    }
    console.log("****");
    console.log(httpPostData);
    let taskList = [];
    let count = 0;
    if (taskFilter) {
      const allTasks = await Axiosplus[httpMethod]({
        path: `filter/${taskFilter.id}/list/?${queryParamBuilder}`,
        values: httpPostData,
        // path: `filter/${taskFilter.id}/list/?firstResult=${resultPointer}&assignee=${user}&assigned=true&maxResults=${offset}`,
        config: {
          headers: { Authorization: authToken, Accept: "application/hal+json" }
        }
      });
      //step3 -> Extract the information we need to filter :
      let {
        _embedded: { caseDefinition, processDefinition, task },
        count: taskCount
      } = allTasks;
      //assign taskCount to count variable
      count = taskCount;
      //check the user has task or not
      //if user has task filter the task based on our need
      if (count > 0) {
        if (task)
          taskList = await Promise.all(
            task.map(async data => {
              //get taskVariables by taskId and append to task data
              let getTaskVariables = await Axiosplus.get({
                path: `task/${data.id}/variables`,
                config: { headers: { Authorization: authToken } }
              });

              //if taskVariables found for each task append it to the data
              data = {
                ...data,
                taskVariables: getTaskVariables ? getTaskVariables : {}
              };

              if (data.processDefinitionId) {
                //if processDefinitionId found append the processDefinition name
                let processDefinitionFinder = processDefinition.find(
                  pf => pf.id === data.processDefinitionId
                );
                return {
                  ...data,
                  processDefinitionName: processDefinitionFinder.name
                    ? processDefinitionFinder.name
                    : processDefinitionFinder.key
                };
              } else if (data.caseDefinitionId) {
                //if caseDefinitionId found append the caseDefinition name
                let caseDefinitionFinder = caseDefinition.find(
                  cf => cf.id === data.caseDefinitionId
                );
                return {
                  ...data,
                  caseDefinitionName: caseDefinitionFinder.name
                    ? caseDefinitionFinder.name
                    : caseDefinitionFinder.key
                };
              }
              return null;
            })
          );
      }
    }
    return {
      filter: taskFilter || {},
      taskList: taskList,
      count
    };
  } catch (error) {
    throw new Error(error);
  }
};
export const resetTask = where => ({
  type: type.RESET_TASKS,
  where
});

export const selectedTask = data => ({
  type: type.SELECTED_TASK,
  data
});

export const updateTask = ({ taskId, values = {}, authToken, taskName }) => {
  return async dispatch => {
    try {
      const data = await Axiosplus.put({
        path: `/task/${taskId}/`,
        values,
        config: {
          headers: { Authorization: authToken, Accept: "application/json" }
        }
      });
      dispatch({
        type: type.UPDATE_TASK,
        data: {
          taskId,
          taskName,
          values
        }
      });
      return data;
    } catch (e) {
      throw e;
    }
  };
};

export const completeTask = ({ taskId, values = {}, authToken, taskName }) => {
  return async dispatch => {
    try {
      const data = await Axiosplus.post({
        path: `/task/${taskId}/submit-form`,
        values,
        config: {
          headers: { Authorization: authToken, Accept: "application/json" }
        }
      });
      dispatch({
        type: type.COMPLETE_TASK,
        data: {
          taskId,
          taskName
        }
      });
      return data;
    } catch (e) {
      throw e;
    }
  };
};
//select some unclaimed task and claim for the current user
export const claimTask = ({ task, values = {}, authToken }) => {
  return async dispatch => {
    try {
      const data = await Axiosplus.post({
        path: `/task/${task.id}/claim`,
        values,
        config: {
          headers: { Authorization: authToken, Accept: "application/json" }
        }
      });
      return data;
    } catch (e) {
      throw e;
    }
  };
};
export const saveTask = data => ({ type: type.SAVE_TASK, data });
export const resetSelectedTask = () => ({ type: type.RESET_SELECTED_TASK });
export const handleTaskFromVisibility = data => ({
  type: type.TASK_FORM_VISIBILITY,
  data
});
//saving the unclaimed tasks that are selected.
export const selectedClaimTasks = data => ({
  type: type.SELECTED_CLAIM_TASKS,
  data
});
//After claiming the unclaimed task remove those tasks from unclaimed tasks tab
export const removeClaimTask = () => ({ type: type.REMOVE_CLAIM_TASK });

export const createComments = ({ taskId, values = {}, authToken }) => {
  return async () => {
    try {
      const data = await Axiosplus.post({
        path: `/task/${taskId}/comment/create`,
        values,
        config: {
          headers: { Authorization: authToken, Accept: "application/json" }
        }
      });
      return data;
    } catch (e) {
      throw e;
    }
  };
};

export const getGroups = ({authToken}) => {
  return async dispatch => {
    let data = await Axiosplus.get({
      path: "/group",
      config: {
        headers: { Authorization: authToken, Accept: "application/json" }
      }
    });
    console.log("After Group Service call =====================", data);
    dispatch({
      type: type.GET_GROUPS,
      data
    })
    return data;
  };
};
