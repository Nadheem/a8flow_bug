import type from "../types";

export const addTabInfo = data => {
  return {
    type: type.ADD_TAB_INFO,
    data
  };
};

export const addIframeReference = data => {
  return {
    type: type.ADD_POINTER,
    data
  };
};

export const addIframeUrl = data =>{
  return {
    type : type.ADD_IFRAME_URL,
    data
  }
}

export const addSelectedTaskToIframe = data => {
  return {
    type: type.ADD_SELECTED_TASK_TO_IFRAME,
    data
  };
};

export const removeIframeConfig = () => {
  return {
    type: type.CLEAR_IFRAME_CONFIG
  };
};

export const setActiveTab = (data)=>{
    return {
        type : type.SET_ACTIVE_TAB,
        data
    }
}

export const handleIframeLoader = (data)=>{
  return {
      type : type.IFRAME_LOADER,
      data
  }
}