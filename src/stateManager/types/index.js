import authTypes from "./auth.type";
import uiTypes from "./ui.type";
import taskTypes from "./task.type";
import iframe from "./iframe.type";
export default {
  ...authTypes,
  ...uiTypes,
  ...taskTypes,
  ...iframe
};
