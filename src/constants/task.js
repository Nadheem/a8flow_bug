export default {
    myTask : "My Tasks",
    myGroupTasks : "My Group Tasks",
    unClaimedTasks : "All Tasks",
    paginationOffsetLimit : 10,
    paginationStartingPointer : 0
}
