import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { PrivateRoute } from "./helpers";
import { SignIn, SignUp, NotFound, Home, ForgotPassword, Dashboard, Settings } from "./components";
import 'antd/dist/antd.css';
// import './assets/css/bootstrap.min.css';
// import "./assets/css/bootstrap.css";
// import './assets/css/icomoon.css';
// import './assets/css/jquery.range.css';
// import './assets/css/flatpickr.css';
import './assets/css/style.css';
// import "./assets/css/app.css";
// import "./assets/css/app.css";
import './App.css';
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/forgotpassword" component={ForgotPassword} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
            <PrivateRoute path="/settings" component={Settings} />
            <PrivateRoute path={["/task/", "/"]} component={Home} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
